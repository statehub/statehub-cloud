module gitlab.com/statehub/statehub-cloud

go 1.16

require (
	github.com/Azure/azure-sdk-for-go v55.2.0+incompatible
	github.com/Azure/go-autorest/autorest v0.11.19
	github.com/Azure/go-autorest/autorest/azure/auth v0.5.7
	github.com/Azure/go-autorest/autorest/to v0.4.0
	github.com/Azure/go-autorest/autorest/validation v0.3.1 // indirect
	github.com/aws/aws-sdk-go v1.41.9
	github.com/aws/aws-sdk-go-v2 v1.11.2
	github.com/aws/aws-sdk-go-v2/config v1.3.0
	github.com/aws/aws-sdk-go-v2/credentials v1.2.1
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.1.1
	github.com/aws/aws-sdk-go-v2/service/ec2 v1.18.0
	github.com/aws/aws-sdk-go-v2/service/elasticloadbalancingv2 v1.13.1
	github.com/aws/smithy-go v1.10.0 // indirect
	github.com/go-resty/resty/v2 v2.4.0
	github.com/google/uuid v1.1.5
	github.com/joho/godotenv v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/afero v1.8.1 // indirect
	github.com/spf13/cobra v1.3.0
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20220207234003-57398862261d // indirect
	golang.org/x/tools/gopls v0.7.4 // indirect
	gopkg.in/ini.v1 v1.66.3 // indirect
)
