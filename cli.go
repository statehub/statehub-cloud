package main

import (
	"context"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/statehub/statehub-cloud/pkg/cloud"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

var (
	envFlags = map[string]string{
		"VENDOR":                "vendor",
		"AZURE_TENANT_ID":       "azureTenantId",
		"AZURE_CLIENT_ID":       "azureClientId",
		"AZURE_CLIENT_SECRET":   "azureClientSecret",
		"AZURE_SUBSCRIPTION_ID": "azureSubscriptionId",
		"AWS_ACCESS_KEY_ID":     "awsAccessKeyId",
		"AWS_SECRET_ACCESS_KEY": "awsSecretAccessKey",
		"AWS_REGION":            "awsDefaultRegion",
	}
	rootCmd = &cobra.Command{
		Use:   "cloud",
		Short: "A CLI tool for interacting with the Statehub Cloud",
		Long: `A CLI tool for interacting with the Statehub Cloud package.

Example:
  $ cloud get-provider
`,
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			for env, flag := range envFlags {
				if viper.GetString(flag) != "" && os.Getenv(env) != viper.GetString(flag) {
					os.Setenv(env, viper.GetString(flag))
				}
			}
		},
	}
	getMetadata = &cobra.Command{
		Use:   "get-metadata",
		Short: "Get instance metadata",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return err
			}

			metadata, err := cloud.GetMetadata()
			if err != nil {
				return fmt.Errorf("could not get metadata: %v", err)
			}

			fmt.Printf("Metadata: %+v\n", metadata)

			return
		},
	}
	getProvider = &cobra.Command{
		Use:   "get-provider",
		Short: "Get Cloud Provider",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return err
			}

			provider := cloud.GetProvider()
			fmt.Println(provider)

			return
		},
	}
	getIdentity = &cobra.Command{
		Use:   "get-identity",
		Short: "Get identity",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return err
			}

			identity, err := cloud.GetCallerIdentity()
			if err != nil {
				return fmt.Errorf("could not get identity: %v", err)
			}

			fmt.Printf("Identity: %+v\n", identity)

			return
		},
	}
	generalPort    = int(0)
	instancesPorts = map[string]int{}
	plsRequest     = types.PlsRequest{}
	createPls      = &cobra.Command{
		Use:    "create-pls",
		Short:  "Create Private Link Service",
		PreRun: preparedPlsRequest,
		RunE: func(cmd *cobra.Command, args []string) (err error) {

			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			pls, err := cloud.CreatePrivateLinkService(plsRequest)
			if err != nil {
				return fmt.Errorf("failed to create Private Link Service: %s", err)
			}

			fmt.Printf("Created Private Link Service: %#v \n", pls)

			return
		},
	}
	getPlsResources = &cobra.Command{
		Use:    "get-pls-resources",
		Short:  "Get Private Link Service Resources",
		PreRun: preparedPlsRequest,
		RunE: func(cmd *cobra.Command, args []string) (err error) {

			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			resources, err := cloud.GetPrivateLinkServiceResources(plsRequest)
			if err != nil {
				return fmt.Errorf("failed to create Private Link Service: %s", err)
			}

			fmt.Printf("Private Link Service resources: %#v \n", resources)

			return
		},
	}
	deletePls = &cobra.Command{
		Use:   "delete-pls",
		Short: "Delete Private Link Service",
		RunE: func(cmd *cobra.Command, args []string) (err error) {

			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			if err := cloud.DeletePrivateLinkService(plsRequest); err != nil {
				return fmt.Errorf("failed to create Private Link Service: %s", err)
			}

			fmt.Println("Deleted Private Link Service")

			return
		},
	}
	createPleRequest = types.CreateEndpointRequest{}
	getPle           = &cobra.Command{
		Use:   "get-endpoint",
		Short: "Get Private Link Endpoint",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			ple, err := cloud.GetEndpoint(&createPleRequest)
			if err != nil {
				return fmt.Errorf("failed to get Private Link Endpoint: %s", err)
			}

			fmt.Printf("Private Link Endpoint: %#v \n", ple)

			return
		},
	}
	createPle = &cobra.Command{
		Use:   "create-endpoint",
		Short: "Create Private Link Endpoint",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			ple, err := cloud.CreateEndpoint(&createPleRequest)
			if err != nil {
				return fmt.Errorf("failed to create Private Link Endpoint: %s", err)
			}

			fmt.Printf("Created Private Link Endpoint: %#v \n", ple)

			return
		},
	}
	deleteEndpointID = ""
	deletePle        = &cobra.Command{
		Use:   "delete-endpoint",
		Short: "Delete Private Link Endpoint",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			if err := cloud.DeleteEndpoint(context.TODO(), deleteEndpointID); err != nil {
				return fmt.Errorf("failed to create Private Link Endpoint: %s", err)
			}

			fmt.Println("Private Link Endpoint deleted")

			return
		},
	}
	deleteSgId = ""
	deleteSg   = &cobra.Command{
		Use:   "delete-sg",
		Short: "Delete Security Group",
		RunE: func(cmd *cobra.Command, args []string) (err error) {
			cloud, err := cloud.GetCloud()
			if err != nil {
				return fmt.Errorf("could not get cloud: %w", err)
			}

			if err := cloud.DeleteSecurityGroup(context.TODO(), deleteSgId); err != nil {
				return fmt.Errorf("failed to delete Security Group: %s", err)
			}

			fmt.Println("Security Group deleted")

			return
		},
	}
)

func preparedPlsRequest(cmd *cobra.Command, args []string) {
	if generalPort > 0 {
		plsRequest.GeneralPort = int32(generalPort)
	}

	plsRequest.InstancesPorts = make(map[string]int32)
	for instance, port := range instancesPorts {
		plsRequest.InstancesPorts[instance] = int32(port)
	}
}

func init() {
	godotenv.Load()

	for _, flag := range envFlags {
		rootCmd.PersistentFlags().String(flag, "", "")
		viper.BindPFlag(flag, rootCmd.PersistentFlags().Lookup(flag))
	}

	rootCmd.AddCommand(getProvider)
	rootCmd.AddCommand(getMetadata)
	rootCmd.AddCommand(getIdentity)

	for _, cmd := range []*cobra.Command{
		createPls,
		getPlsResources,
		deletePls,
	} {
		rootCmd.AddCommand(cmd)

		cmd.PersistentFlags().StringVar(&plsRequest.Name, "name", "", "")
		cmd.PersistentFlags().StringVar(&plsRequest.VpcId, "vpcId", "", "")
		cmd.Flags().StringSliceVar(&plsRequest.Subnets, "subnets", []string{}, "")
		cmd.Flags().IntVar(&generalPort, "generalPort", 0, "")
		cmd.Flags().StringToIntVar(&instancesPorts, "instancesPorts", map[string]int{}, "")
		cmd.Flags().StringToStringVar(&plsRequest.Tags, "tags", map[string]string{}, "")
	}

	for _, cmd := range []*cobra.Command{
		createPle,
		getPle,
	} {
		rootCmd.AddCommand(cmd)

		cmd.Flags().StringVar(&createPleRequest.StateID, "state-id", "", "")
		cmd.Flags().StringVar(&createPleRequest.StateName, "state-name", "", "")
		cmd.Flags().StringVar(&createPleRequest.Region, "region", "", "")
		cmd.Flags().StringVar(&createPleRequest.AvailabilityZone, "availability-zone", "", "")
		cmd.Flags().StringVar(&createPleRequest.Vnet, "vnet", "", "")
		cmd.Flags().StringVar(&createPleRequest.Subnet, "subnet", "", "")
		cmd.Flags().StringVar(&createPleRequest.ProviderScope, "provider-scope", "", "")
		cmd.Flags().StringVar(&createPleRequest.ServiceURI, "service-uri", "", "")
		cmd.Flags().StringVar(&createPleRequest.ResourceGroup, "resource-group", "", "")
		cmd.Flags().BoolVar(&createPleRequest.DryRun, "dry-run", false, "")
	}

	rootCmd.AddCommand(deletePle)
	deletePle.Flags().StringVar(&deleteEndpointID, "endpoint-id", "", "")

	rootCmd.AddCommand(deleteSg)
	deleteSg.Flags().StringVar(&deleteSgId, "security-group-id", "", "")
}

func main() {
	rootCmd.Execute()
}
