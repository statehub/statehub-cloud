/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cloud

import (
	"context"
	"fmt"
	"os"
	"strings"

	"gitlab.com/statehub/statehub-cloud/pkg/aws"
	"gitlab.com/statehub/statehub-cloud/pkg/azure"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

type Cloud interface {
	GetMetadata() (types.Metadata, error)
	// Returns nil, nil if not found
	GetEndpoint(request *types.CreateEndpointRequest) (*types.Endpoint, error)
	CreateEndpoint(*types.CreateEndpointRequest) (*types.Endpoint, error)
	DeleteEndpoint(ctx context.Context, endpointID string) error
	DeleteSecurityGroup(ctx context.Context, securityGroupID string) error
	GetProvider() types.Vendor
	GetCallerIdentity() (*string, error)
	LoadCredentials()
	CreatePrivateLinkService(types.PlsRequest) (types.PlsResponse, error)
	DeletePrivateLinkService(types.PlsRequest) error
	GetPrivateLinkServiceResources(types.PlsRequest) (types.PlsResourcesResponse, error)
}

func GetCloud() (cloud Cloud, err error) {
	vendor, err := getVendorFromEnv()
	if err != nil {
		return cloud, err
	}

	// No vendor specified in environment, try understand by instance metadata
	if vendor == nil {
		// Trying to determine if we are in azure
		if _, err := azure.InstanceMetadata(); err == nil {
			v := types.Azure // Pointer to constant cannot be made
			vendor = &v
		} else if _, err := aws.InstanceMetadata(); err == nil {
			v := types.Aws // Pointer to constant cannot be made
			vendor = &v
		} else {
			return cloud, fmt.Errorf("could not understand vendor by instance metadata")
		}
	}

	switch *vendor {
	case types.Azure:
		return azure.New()
	case types.Aws:
		return aws.New()
	default:
		return cloud, fmt.Errorf("unknown vendor: %s", *vendor)
	}
}

func getVendorFromEnv() (*types.Vendor, error) {
	vendorEnv, ok := os.LookupEnv("VENDOR")
	if !ok {
		return nil, nil
	}

	vendor := types.Vendor(strings.ToLower(vendorEnv))
	switch vendor {
	case types.Aws, types.Azure:
		return &vendor, nil
	}

	return nil, fmt.Errorf("unknown vendor in environment: %s", vendor)
}
