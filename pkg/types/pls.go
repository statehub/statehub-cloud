package types

type PlsRequest struct {
	Name           string
	VpcId          string
	Subnets        []string
	GeneralPort    int32
	InstancesPorts map[string]int32
	Tags           map[string]string
}

type PlsResponse struct {
	Id string
	Name string
}

type PlsResourceType string

const (
	PlsResourceSubnet   PlsResourceType = "subnet"
	PlsResourceLB       PlsResourceType = "loadBalancer"
	PlsResourceNic      PlsResourceType = "networkInterface"
	PlsResourcePls      PlsResourceType = "privateLinkService"
	PlsResourceTg       PlsResourceType = "targetGroup"
	PlsResourceListener PlsResourceType = "listener"
)

type PlsResource struct {
	Type PlsResourceType
	Id   string
}

type PlsResourcesResponse struct {
	Resources []PlsResource
}

func (r *PlsResourcesResponse) AddResource(rType PlsResourceType, id string) {
	r.Resources = append(r.Resources, PlsResource{
		Type: rType,
		Id:   id,
	})
}
