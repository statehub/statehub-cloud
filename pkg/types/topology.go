package types

// constants for statehub topology labels
const (
	StatehubTopologyPrefix             = "topology.statehub.csi"
	StatehubProviderTopologyLabel      = "topology.statehub.csi/provider"
	StatehubProviderScopeTopologyLabel = "topology.statehub.csi/providerScope"
	StatehubRegionTopologyLabel        = "topology.statehub.csi/region"
	StatehubVnetTopologyLabel          = "topology.statehub.csi/vnet"
	StatehubSubnetTopologyLabel        = "topology.statehub.csi/subnet"
	StatehubStatusTopologyLabel        = "topology.statehub.csi/status"
	StatehubResourceGroupTopologyLabel = "topology.statehub.csi/resourceGroup"
)
