package types

type CreateEndpointRequest struct {
	StateID          string
	StateName        string
	DryRun           bool
	Region           string
	AvailabilityZone string
	Vnet             string
	Subnet           string
	ProviderScope    string
	ServiceURI       string
	ResourceGroup    string
}

type Endpoint struct {
	Address         string `json:"address"`
	Ready           bool   `json:"ready"`
	SecurityGroupID string `json:"security_group_id"`
	ID              string `json:"id"`
}
