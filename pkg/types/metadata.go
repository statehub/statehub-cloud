package types

// Vendor definition
type Vendor string

// Typed constants
const (
	Aws     Vendor = "aws"
	Azure   Vendor = "azure"
	Unknown Vendor = "unknown"
)

type Metadata struct {
	InstanceID       string
	Region           string
	AvailabilityZone string
	Vnet             string
	Subnet           string
	CloudProvider    string
	ProviderScope    string
	ResourceGroup    string
}
