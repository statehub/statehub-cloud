package aws

// Instance - Aws instance metadata
type Instance struct {
	AmiID              string             `json:"ami-id"`
	AmiLaunchIndex     string             `json:"ami-launch-index"`
	AmiManifestPath    string             `json:"ami-manifest-path"`
	BlockDeviceMapping BlockDeviceMapping `json:"block-device-mapping"`
	Events             Events             `json:"events"`
	Hostname           string             `json:"hostname"`
	IAM                IAM                `json:"iam"`
	InstanceAction     string             `json:"instance-action"`
	InstanceID         string             `json:"instance-id"`
	InstanceLifeCycle  string             `json:"instance-life-cycle"`
	InstanceType       string             `json:"instance-type"`
	KernelID           string             `json:"kernel-id"`
	LocalHostname      string             `json:"local-hostname"`
	LocalIPv4          string             `json:"local-ipv4"`
	Mac                string             `json:"mac"`
	Network            Network            `json:"network"`
	Placement          Placement          `json:"placement"`
	ProductCodes       string             `json:"product-codes"`
	PublicHostname     string             `json:"public-hostname"`
	PublicIPv4         string             `json:"public-ipv4"`
	RamdiskID          string             `json:"ramdisk-id"`
	ReservationID      string             `json:"reservation-id"`
	SecurityGroups     string             `json:"security-groups"`
	Services           Services           `json:"services"`
	Spot               Spot               `json:"spot"`
}

// Subnets - get subnets
func (i *Instance) Subnets() []string {
	subnets := []string{}
	for _, mac := range i.Network.Interfaces.Macs {
		subnets = append(subnets, mac.SubnetID)

	}
	return subnets
}

func (i *Instance) VPCs() []string {
	VPCs := []string{}
	for _, mac := range i.Network.Interfaces.Macs {
		VPCs = append(VPCs, mac.VPCID)

	}
	return VPCs
}

// BlockDeviceMapping - Block device mapping
type BlockDeviceMapping struct {
	Ami        string `json:"ami"`
	Ebs1       string `json:"ebs1"`
	Ebs2       string `json:"ebs2"`
	Ebs3       string `json:"ebs3"`
	Ebs4       string `json:"ebs4"`
	Ebs5       string `json:"ebs5"`
	Ebs6       string `json:"ebs6"`
	Ebs7       string `json:"ebs7"`
	Ephemeral0 string `json:"ephemeral0"`
	Ephemeral1 string `json:"ephemeral1"`
	Ephemeral2 string `json:"ephemeral2"`
	Ephemeral3 string `json:"ephemeral3"`
	Root       string `json:"root"`
	Swap       string `json:"swap"`
}

// Events events
type Events struct {
	Maintenance Maintenance `json:"maintenance"`
}

// Maintenance description
type Maintenance struct {
	History   string `json:"history"`
	Scheduled string `json:"scheduled"`
}

// IAM role
type IAM struct {
	Info string `json:"info"`
}

// Network description
type Network struct {
	Interfaces Interfaces `json:"interfaces"`
}

// Interfaces description
type Interfaces struct {
	Macs map[string]Mac `json:"macs"`
}

// Mac - Mac interface description
type Mac struct {
	DeviceNumber     string `json:"device-number"`
	InterfaceID      string `json:"interface-id"`
	LocalHostname    string `json:"local-hostname"`
	Mac              string `json:"mac"`
	NetworkCardIndex string `json:"network-card-index"`
	OwnerID          string `json:"owner-id"`
	PublicHostname   string `json:"public-hostname"`
	SecurityGroups   string `json:"security-groups"`
	SubnetID         string `json:"subnet-id"`
	VPCID            string `json:"vpc-id"`
}

// Placement info
type Placement struct {
	AvailabilityZone   string `json:"availability-zone"`
	AvailabilityZoneID string `json:"availability-zone-id"`
	GroupName          string `json:"group-name"`
	HostID             string `json:"host-id"`
	PartitionNumber    int    `json:"partition-number"`
	Region             string `json:"region"`
}

// Services info
type Services struct {
	Domain    string `json:"domain"`
	Partition string `json:"partition"`
}

// Spot info
type Spot struct {
	InstanceAction  string `json:"instance-action"`
	TerminationTime string `json:"termination-time"`
}

// DynamicData info
type DynamicData struct {
	InstanceIdentity InstanceIdentity `json:"instance-identity"`
}

// InstanceIdentity path
type InstanceIdentity struct {
	Document  Document `json:"document"`
	Dsa2048   string   `json:"dsa2048"`
	Pkcs7     string   `json:"pkcs7"`
	Rsa2048   string   `json:"rsa2048"`
	Signature string   `json:"signature"`
}

// Document holds more verbose information about the instance like accountID.
type Document struct {
	DevpayProductCodes      string   `json:"devpayProductCodes"`
	MarketplaceProductCodes []string `json:"marketplaceProductCodes"`
	AvailabilityZone        string   `json:"availabilityZone"`
	PrivateIP               string   `json:"privateIp"`
	Version                 string   `json:"version"`
	InstanceID              string   `json:"instanceId"`
	BillingProducts         string   `json:"billingProducts"`
	InstanceType            string   `json:"instanceType"`
	AccountID               string   `json:"accountId"`
	ImageID                 string   `json:"imageId"`
	PendingTime             string   `json:"pendingTime"`
	Architecture            string   `json:"architecture"`
	KernelID                string   `json:"kernelId"`
	RamdiskID               string   `json:"ramdiskId"`
	Region                  string   `json:"region"`
}
