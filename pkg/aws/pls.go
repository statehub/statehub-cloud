package aws

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	ec2Types "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	elbv2 "github.com/aws/aws-sdk-go-v2/service/elasticloadbalancingv2"
	elbv2Types "github.com/aws/aws-sdk-go-v2/service/elasticloadbalancingv2/types"
	"github.com/sirupsen/logrus"
	"gitlab.com/statehub/statehub-cloud/pkg/helpers"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

/*
 * Services
 */
type plsService struct {
	log *logrus.Entry
	req types.PlsRequest
	elb plsElb
	ec2 plsEc2
}

func initPlsService(
	ctx context.Context,
	clients Clients,
	req types.PlsRequest,
	log *logrus.Entry,
) plsService {
	log = log.WithField("service", "privatelinkservice")

	return plsService{
		log: log,
		req: req,
		elb: newPlsElb(ctx, clients.elb, log, req),
		ec2: newPlsEc2(ctx, clients.ec2, log, req),
	}
}

// 1. Create ELB
// 2. Create General target group
// 3. Create general listener
// 4. Create target group for each port
// 5. Create listener for each port
// 6. Register general target group for node
// 7. Register port target group for node
// 8. Create vpc
func (svc plsService) Create() (res types.PlsResponse, err error) {
	svc.log.Debugf("creating pls")

	// Load Balancer
	elb, err := svc.elb.findOrCreateElb()
	if err != nil {
		return res, fmt.Errorf("failed to find or create elb: %+v", err)
	}

	// General Port Target Group
	generalTargetGroup, err := svc.elb.findOrCreateTargetGroup(svc.req.GeneralPort)
	if err != nil {
		return res, fmt.Errorf("failed to find or create general target group: %+v", err)
	}

	// General Port Listener
	if _, err = svc.elb.findOrCreateListener(
		aws.ToString(elb.LoadBalancerArn),
		svc.req.GeneralPort,
		aws.ToString(generalTargetGroup.TargetGroupArn),
	); err != nil {
		return res, fmt.Errorf("failed to find or create general listener: %+v", err)
	}

	for instanceId, port := range svc.req.InstancesPorts {
		// Instance Target Group
		instanceTargetGroup, err := svc.elb.findOrCreateTargetGroup(port)
		if err != nil {
			return res, fmt.Errorf("failed to find or create instance target group: %+v", err)
		}

		// Instance Port Listener
		if _, err = svc.elb.findOrCreateListener(
			aws.ToString(elb.LoadBalancerArn),
			port,
			aws.ToString(instanceTargetGroup.TargetGroupArn),
		); err != nil {
			return res, fmt.Errorf("failed to find or create instance listener: %+v", err)
		}

		// Register general port for instance target group
		if _, err := svc.elb.checkOrRegisterTarget(
			instanceId,
			svc.req.GeneralPort,
			aws.ToString(generalTargetGroup.TargetGroupArn),
		); err != nil {
			return res, fmt.Errorf("failed to register general port for instance target group: %+v", err)
		}

		// Register instance port for instance target group
		if _, err := svc.elb.checkOrRegisterTarget(
			instanceId,
			port,
			aws.ToString(instanceTargetGroup.TargetGroupArn),
		); err != nil {
			return res, fmt.Errorf("failed to register instance port for instance target group: %+v", err)
		}
	}

	pls, err := svc.ec2.findOrCreatePls(aws.ToString(elb.LoadBalancerArn))
	if err != nil {
		return res, fmt.Errorf("failed to find or create pls: %+v", err)
	}

	res.Id = aws.ToString(pls.ServiceId)
	res.Name = aws.ToString(pls.ServiceName)

	svc.log.Debugf("created pls: %+v", res.Id)

	return
}

// 1. Reject vpc connections
// 2. Delete vpc
// 3. Unregister general port target group for node
// 4. Unregister instance port target group for node
// 5. Delete listener for each port
// 6. Delete target group for each port
// 7. Delete general listener
// 8. Delete General target group
// 9. Delete ELB
func (svc *plsService) Delete() (err error) {
	svc.log.Debugf("deleting pls")

	// Private Link Service
	if err = svc.ec2.findAndDeletePls(); err != nil {
		return fmt.Errorf("failed to find and delete pls: %+v", err)
	}

	// Load Balancer
	elb, err := svc.elb.findElb()
	if err != nil {
		return fmt.Errorf("failed to find elb: %+v", err)
	}

	if elb != nil {
		// General Port Listener
		if err = svc.elb.findAndDeleteListener(
			aws.ToString(elb.LoadBalancerArn),
			svc.req.GeneralPort,
		); err != nil {
			return fmt.Errorf("failed to find and delete general listener: %+v", err)
		}
	}

	// General Port Target Group
	generalTargetGroup, err := svc.elb.findTargetGroup(svc.req.GeneralPort)
	if err != nil {
		return fmt.Errorf("failed to find general target group: %+v", err)
	}

	for instanceId, port := range svc.req.InstancesPorts {
		if elb != nil {
			// General Port Listener
			if err = svc.elb.findAndDeleteListener(
				aws.ToString(elb.LoadBalancerArn),
				port,
			); err != nil {
				return fmt.Errorf("failed to find and delete instance listener: %+v", err)
			}
		}

		if generalTargetGroup != nil {
			if err := svc.elb.checkAndDeregisterTarget(
				instanceId,
				port,
				aws.ToString(generalTargetGroup.TargetGroupArn),
			); err != nil {
				return fmt.Errorf("failed to unregister instance port for instance target group: %+v", err)
			}
		}

		// Instance Target Group
		instanceTargetGroup, err := svc.elb.findTargetGroup(port)
		if err != nil {
			return fmt.Errorf("failed to find instance target group: %+v", err)
		}

		if instanceTargetGroup != nil {
			if err := svc.elb.checkAndDeregisterTarget(
				instanceId,
				port,
				aws.ToString(instanceTargetGroup.TargetGroupArn),
			); err != nil {
				return fmt.Errorf("failed to unregister instance port for instance target group: %+v", err)
			}

			if err := svc.elb.deleteTargetGroup(aws.ToString(instanceTargetGroup.TargetGroupArn)); err != nil {
				return fmt.Errorf("failed to delete instance target group: %+v", err)
			}
		}
	}

	if generalTargetGroup != nil {
		if err := svc.elb.deleteTargetGroup(aws.ToString(generalTargetGroup.TargetGroupArn)); err != nil {
			return fmt.Errorf("failed to delete general target group: %+v", err)
		}
	}

	if elb != nil {
		if err := svc.elb.deleteElb(aws.ToString(elb.LoadBalancerArn)); err != nil {
			return fmt.Errorf("failed to delete elb: %+v", err)
		}
	}

	svc.log.Debugf("deleted pls")

	return
}

func (svc *plsService) GetResources() (res types.PlsResourcesResponse, err error) {
	svc.log.Debugf("getting pls resources")

	// Load Balancer
	elb, err := svc.elb.findElb()
	if err != nil {
		return res, fmt.Errorf("failed to find elb: %+v", err)
	}

	if elb != nil {
		res.AddResource(types.PlsResourceLB, aws.ToString(elb.LoadBalancerArn))
	}

	// General Port Target Group
	generalTargetGroup, err := svc.elb.findTargetGroup(svc.req.GeneralPort)
	if err != nil {
		return res, fmt.Errorf("failed to find general target group: %+v", err)
	}

	if generalTargetGroup != nil {
		res.AddResource(types.PlsResourceTg, aws.ToString(generalTargetGroup.TargetGroupArn))
	}

	if elb != nil {
		// General Port Listener
		generalListener, err := svc.elb.findListener(aws.ToString(elb.LoadBalancerArn), svc.req.GeneralPort)
		if err != nil {
			return res, fmt.Errorf("failed to find general listener: %+v", err)
		}

		if generalListener != nil {
			res.AddResource(types.PlsResourceListener, aws.ToString(generalListener.ListenerArn))
		}
	}

	for _, port := range svc.req.InstancesPorts {
		// Instance Target Group
		instanceTargetGroup, err := svc.elb.findTargetGroup(port)
		if err != nil {
			return res, fmt.Errorf("failed to find instance target group: %+v", err)
		}

		if instanceTargetGroup != nil {
			res.AddResource(types.PlsResourceTg, aws.ToString(instanceTargetGroup.TargetGroupArn))
		}

		if elb != nil {
			// Instance Port Listener
			instanceListener, err := svc.elb.findListener(aws.ToString(elb.LoadBalancerArn), port)
			if err != nil {
				return res, fmt.Errorf("failed to find instance listener: %+v", err)
			}

			if instanceListener != nil {
				res.AddResource(types.PlsResourceListener, aws.ToString(instanceListener.ListenerArn))
			}
		}
	}

	pls, err := svc.ec2.findPls()
	if err != nil {
		return res, fmt.Errorf("failed to find pls: %+v", err)
	}

	if pls != nil {
		res.AddResource(types.PlsResourcePls, aws.ToString(pls.ServiceId))
	}

	svc.log.Debugf("got pls resources: %+v", res)

	return res, nil
}

/*
 * Pls Elb Service
 */

type plsElb struct {
	ctx    context.Context
	client ElbClient
	log    *logrus.Entry
	req    types.PlsRequest
	tags   []elbv2Types.Tag
}

func newPlsElb(ctx context.Context, client ElbClient, log *logrus.Entry, req types.PlsRequest) plsElb {
	var elbTags []elbv2Types.Tag
	for key, value := range req.Tags {
		key := key
		value := value
		elbTags = append(elbTags, elbv2Types.Tag{Key: &key, Value: &value})
	}

	return plsElb{
		ctx:    ctx,
		client: client,
		log:    log.WithField("service", "loadbalancer"),
		req:    req,
		tags:   elbTags,
	}
}

func (svc plsElb) findOrCreateElb() (resource elbv2Types.LoadBalancer, err error) {
	found, err := svc.findElb()
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createElb()
	}

	return resource, err
}

func (svc plsElb) findElb() (resource *elbv2Types.LoadBalancer, err error) {
	output, err := svc.client.DescribeLoadBalancers(svc.ctx, &elbv2.DescribeLoadBalancersInput{
		Names: []string{svc.req.Name},
	})

	if err != nil {
		var notFoundErr *elbv2Types.LoadBalancerNotFoundException
		if errors.As(err, &notFoundErr) {
			svc.log.Debugf("elastic load balancer %s not found", svc.req.Name)
			return nil, nil
		}

		return nil, fmt.Errorf("failed to describe load balancer: %+v", err)
	}

	resource = &output.LoadBalancers[0]

	if resource.State.Code == elbv2Types.LoadBalancerStateEnumProvisioning {
		if err := svc.waitUntilElbActive(aws.ToString(resource.LoadBalancerArn)); err != nil {
			return nil, err
		}
	}

	svc.log.Debugf("found elastic load balancer: %+v", *resource)

	return
}

func (svc plsElb) createElb() (resource elbv2Types.LoadBalancer, err error) {
	output, err := svc.client.CreateLoadBalancer(svc.ctx, &elbv2.CreateLoadBalancerInput{
		Name:    aws.String(svc.req.Name),
		Scheme:  "internal",
		Type:    "network",
		Subnets: svc.req.Subnets,
		Tags:    svc.tags,
	})

	if err != nil {
		return resource, err
	}

	resource = output.LoadBalancers[0]

	if err := svc.waitUntilElbActive(*resource.LoadBalancerArn); err != nil {
		return resource, err
	}

	svc.log.Debugf("created elastic load balancer: %+v", resource)

	return
}

func (svc plsElb) deleteElb(lbArn string) (err error) {
	if _, err := svc.client.DeleteLoadBalancer(svc.ctx, &elbv2.DeleteLoadBalancerInput{
		LoadBalancerArn: aws.String(lbArn),
	}); err != nil {
		return fmt.Errorf("failed to delete elastic load balancer: %+v", err)
	}

	svc.log.Debugf("deleted elastic load balancer: %s", lbArn)

	return
}

func (svc plsElb) waitUntilElbActive(LoadBalancerArn string) error {
	maxAttempts := 60

	for attempt := 1; ; attempt++ {
		output, err := svc.client.DescribeLoadBalancers(svc.ctx, &elbv2.DescribeLoadBalancersInput{
			LoadBalancerArns: []string{LoadBalancerArn},
		})

		if err != nil {
			return err
		}

		elb := output.LoadBalancers[0]

		if elb.State.Code == elbv2Types.LoadBalancerStateEnumActive {
			return nil
		} else if elb.State.Code == elbv2Types.LoadBalancerStateEnumFailed {
			return fmt.Errorf("failed to create loadBalancer: " + LoadBalancerArn)
		}

		if attempt == maxAttempts {
			break
		}

		time.Sleep(helpers.Backoff.Duration(attempt))
	}

	return fmt.Errorf("exceeded wait attempts: %d", maxAttempts)
}

func (svc plsElb) findOrCreateTargetGroup(port int32) (resource elbv2Types.TargetGroup, err error) {
	found, err := svc.findTargetGroup(port)
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createTargetGroup(port)
	}

	return resource, err
}

func (svc plsElb) findTargetGroup(port int32) (resource *elbv2Types.TargetGroup, err error) {
	tgName := svc.tgName(port)
	output, err := svc.client.DescribeTargetGroups(svc.ctx, &elbv2.DescribeTargetGroupsInput{
		Names: []string{tgName},
	})

	if err != nil {
		var notFoundErr *elbv2Types.TargetGroupNotFoundException
		if errors.As(err, &notFoundErr) {
			svc.log.Debugf("target group %s not found", tgName)
			return nil, nil
		}

		return nil, err
	}

	resource = &output.TargetGroups[0]

	svc.log.Debugf("found target group: %+v", *resource)

	return
}

func (svc plsElb) createTargetGroup(port int32) (resource elbv2Types.TargetGroup, err error) {
	tgName := svc.tgName(port)
	output, err := svc.client.CreateTargetGroup(svc.ctx, &elbv2.CreateTargetGroupInput{
		Name:                       aws.String(tgName),
		Port:                       aws.Int32(int32(port)),
		Protocol:                   "TCP",
		TargetType:                 "instance",
		VpcId:                      &svc.req.VpcId,
		HealthCheckIntervalSeconds: aws.Int32(10),
		HealthyThresholdCount:      aws.Int32(2),
		UnhealthyThresholdCount:    aws.Int32(2),
		Tags:                       svc.tags,
	})

	if err != nil {
		return resource, err
	}

	resource = output.TargetGroups[0]

	svc.log.Debugf("created target group: %+v", resource)

	return
}

func (svc plsElb) deleteTargetGroup(tgArn string) (err error) {
	if _, err := svc.client.DeleteTargetGroup(svc.ctx, &elbv2.DeleteTargetGroupInput{
		TargetGroupArn: aws.String(tgArn),
	}); err != nil {
		return fmt.Errorf("failed to delete target group: %s", tgArn)
	}

	svc.log.Debugf("deleted target group: %s", tgArn)

	return
}

func (svc plsElb) tgName(port int32) string {
	return fmt.Sprintf("%s-%d", svc.req.Name, port)
}

func (svc plsElb) findOrCreateListener(lbArn string, port int32, tgArn string) (resource elbv2Types.Listener, err error) {
	found, err := svc.findListener(lbArn, port)
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createListener(lbArn, port, tgArn)
	}

	return resource, err
}

func (svc plsElb) findAndDeleteListener(lbArn string, port int32) (err error) {
	found, err := svc.findListener(lbArn, port)
	if err != nil {
		return fmt.Errorf("failed to find listener: %s", err)
	}

	if found == nil {
		return
	}

	return svc.deleteListener(aws.ToString(found.ListenerArn))
}

func (svc plsElb) findListener(lbArn string, port int32) (resource *elbv2Types.Listener, err error) {
	output, err := svc.client.DescribeListeners(svc.ctx, &elbv2.DescribeListenersInput{
		LoadBalancerArn: aws.String(lbArn),
	})

	if err != nil {
		var notFoundErr *elbv2Types.ListenerNotFoundException
		if errors.As(err, &notFoundErr) {
			svc.log.Debugf("listeners for load balancer %s not found", lbArn)
			return nil, nil
		}

		return nil, err
	}

	for _, listener := range output.Listeners {
		if aws.ToInt32(listener.Port) == port {
			resource = &listener
			svc.log.Debugf("found listener: %+v", listener)
			break
		}
	}

	return
}

func (svc plsElb) createListener(lbArn string, port int32, tgArn string) (resource elbv2Types.Listener, err error) {
	output, err := svc.client.CreateListener(svc.ctx, &elbv2.CreateListenerInput{
		DefaultActions: []elbv2Types.Action{
			{
				TargetGroupArn: aws.String(tgArn),
				Type:           "forward",
			},
		},
		LoadBalancerArn: aws.String(lbArn),
		Port:            aws.Int32(int32(port)),
		Protocol:        "TCP",
		Tags:            svc.tags,
	})
	if err != nil {
		return resource, err
	}

	resource = output.Listeners[0]

	svc.log.Debugf("created target group: %+v", resource)

	return
}

func (svc plsElb) deleteListener(listenerArn string) (err error) {
	if _, err := svc.client.DeleteListener(svc.ctx, &elbv2.DeleteListenerInput{
		ListenerArn: aws.String(listenerArn),
	}); err != nil {
		return fmt.Errorf("failed to delete listener: %s", listenerArn)
	}

	svc.log.Debugf("deleted listener: %s", listenerArn)

	return
}

func (svc plsElb) checkOrRegisterTarget(instanceId string, port int32, tgArn string) (registered bool, err error) {
	registered, err = svc.checkTarget(instanceId, port, tgArn)
	if err != nil {
		return registered, err
	}

	if registered {
		return true, nil
	}

	return svc.registerTarget(instanceId, port, tgArn)
}

func (svc plsElb) checkAndDeregisterTarget(instanceId string, port int32, tgArn string) (err error) {
	registered, err := svc.checkTarget(instanceId, port, tgArn)
	if err != nil {
		return fmt.Errorf("failed to check target: %w", err)
	}

	if registered {
		return svc.deregisterTarget(instanceId, port, tgArn)
	}

	return nil
}

func (svc plsElb) checkTarget(instanceId string, port int32, tgArn string) (registered bool, err error) {
	targetDesc := elbv2Types.TargetDescription{
		Id:   aws.String(instanceId),
		Port: aws.Int32(int32(port)),
	}

	targetHealth, err := svc.client.DescribeTargetHealth(svc.ctx, &elbv2.DescribeTargetHealthInput{
		TargetGroupArn: aws.String(tgArn),
		Targets:        []elbv2Types.TargetDescription{targetDesc},
	})

	if err != nil {
		return registered, err
	}

	for _, target := range targetHealth.TargetHealthDescriptions {
		if helpers.IsEqStrings(targetDesc.Id, target.Target.Id) &&
			helpers.IsEqInt32(targetDesc.Port, target.Target.Port) {
			return target.TargetHealth.Reason != elbv2Types.TargetHealthReasonEnumNotRegistered, nil
		}
	}

	return false, nil
}

func (svc plsElb) registerTarget(instanceId string, port int32, tgArn string) (registered bool, err error) {
	targetDesc := elbv2Types.TargetDescription{
		Id:   aws.String(instanceId),
		Port: aws.Int32(int32(port)),
	}

	_, err = svc.client.RegisterTargets(svc.ctx, &elbv2.RegisterTargetsInput{
		TargetGroupArn: aws.String(tgArn),
		Targets:        []elbv2Types.TargetDescription{targetDesc},
	})
	if err != nil {
		return registered, err
	}

	svc.log.Debugf("registered target in target group %s for port %d for instance %s", tgArn, port, instanceId)

	return true, nil
}

func (svc plsElb) deregisterTarget(instanceId string, port int32, tgArn string) (err error) {
	if _, err = svc.client.DeregisterTargets(svc.ctx, &elbv2.DeregisterTargetsInput{
		TargetGroupArn: aws.String(tgArn),
		Targets: []elbv2Types.TargetDescription{
			{
				Id:   aws.String(instanceId),
				Port: aws.Int32(int32(port)),
			},
		},
	}); err != nil {
		return fmt.Errorf("failed to deregister target %s from target group %s: %w", instanceId, tgArn, err)
	}

	svc.log.Debugf("deregistered target %s from target group %s", instanceId, tgArn)

	return
}

/*
 * Pls Ec2 Service
 */

type plsEc2 struct {
	ctx    context.Context
	client Ec2Client
	log    *logrus.Entry
	req    types.PlsRequest
	tags   []ec2Types.Tag
}

func newPlsEc2(ctx context.Context, client Ec2Client, log *logrus.Entry, req types.PlsRequest) plsEc2 {
	var elbTags []ec2Types.Tag
	for key, value := range req.Tags {
		key := key
		value := value
		elbTags = append(elbTags, ec2Types.Tag{Key: &key, Value: &value})
	}

	return plsEc2{
		ctx:    ctx,
		client: client,
		log:    log.WithField("service", "ec2"),
		req:    req,
		tags:   elbTags,
	}
}

func (svc plsEc2) findOrCreatePls(lbArn string) (resource ec2Types.ServiceConfiguration, err error) {
	found, err := svc.findPls()
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createPls(lbArn)
	}

	return resource, err
}

func (svc plsEc2) findAndDeletePls() (err error) {
	found, err := svc.findPls()
	if err != nil {
		return err
	}

	if found == nil {
		return nil
	}

	if err = svc.findAndRejectAllPecs(aws.ToString(found.ServiceId)); err != nil {
		return err
	}

	return svc.deletePls(aws.ToString(found.ServiceId))
}

func (svc plsEc2) findPls() (resource *ec2Types.ServiceConfiguration, err error) {
	output, err := svc.client.DescribeVpcEndpointServiceConfigurations(svc.ctx, &ec2.DescribeVpcEndpointServiceConfigurationsInput{
		Filters: []ec2Types.Filter{
			{
				Name:   aws.String("tag:Name"),
				Values: []string{svc.req.Name},
			},
		},
	})

	if output == nil || len(output.ServiceConfigurations) == 0 {
		svc.log.Debugf("pls %s not found", svc.req.Name)
		return nil, nil
	}

	resource = &output.ServiceConfigurations[0]

	svc.log.Debugf("found pls: %+v", *resource)

	return
}

func (svc plsEc2) createPls(lbArn string) (resource ec2Types.ServiceConfiguration, err error) {
	output, err := svc.client.CreateVpcEndpointServiceConfiguration(svc.ctx, &ec2.CreateVpcEndpointServiceConfigurationInput{
		AcceptanceRequired:      aws.Bool(true),
		NetworkLoadBalancerArns: []string{lbArn},
		TagSpecifications: []ec2Types.TagSpecification{{
			ResourceType: ec2Types.ResourceTypeVpcEndpointService,
			Tags: append(
				svc.tags,
				ec2Types.Tag{
					Key:   aws.String("Name"),
					Value: aws.String(svc.req.Name),
				},
			),
		}},
	})

	if err != nil {
		return resource, err
	}

	resource = *output.ServiceConfiguration

	svc.log.Debugf("created pls: %+v", resource)

	return
}

func (svc plsEc2) deletePls(serviceId string) (err error) {
	output, err := svc.client.DeleteVpcEndpointServiceConfigurations(svc.ctx, &ec2.DeleteVpcEndpointServiceConfigurationsInput{
		ServiceIds: []string{serviceId},
	})
	if err != nil {
		return err
	}

	for _, s := range output.Unsuccessful {
		if *s.ResourceId == serviceId {
			return fmt.Errorf("unable to delete pls %s: %s", serviceId, *s.Error.Message)
		}
	}

	svc.log.Debugf("deleted pls: %s", serviceId)

	return
}

func (svc plsEc2) findAndRejectAllPecs(serviceId string) (err error) {
	pecs, err := svc.findPecs(serviceId)
	if err != nil {
		return fmt.Errorf("unable to find pecs for pls %s: %s", serviceId, err)
	}

	if len(pecs) == 0 {
		return nil
	}

	if err = svc.rejectPecs(serviceId, pecs); err != nil {
		return err
	}

	return
}

func (svc plsEc2) findPecs(serviceId string) (pecs []ec2Types.VpcEndpointConnection, err error) {
	output, err := svc.client.DescribeVpcEndpointConnections(svc.ctx, &ec2.DescribeVpcEndpointConnectionsInput{
		Filters: []ec2Types.Filter{
			{
				Name:   aws.String("service-id"),
				Values: []string{serviceId},
			},
		},
	})

	if err != nil {
		return pecs, err
	}

	pecs = output.VpcEndpointConnections

	return
}

func (svc plsEc2) rejectPecs(serviceId string, pecs []ec2Types.VpcEndpointConnection) (err error) {
	vpcEndpointIds := make([]string, len(pecs))
	for i, pec := range pecs {
		vpcEndpointIds[i] = aws.ToString(pec.VpcEndpointId)
	}

	output, err := svc.client.RejectVpcEndpointConnections(svc.ctx, &ec2.RejectVpcEndpointConnectionsInput{
		ServiceId:      aws.String(serviceId),
		VpcEndpointIds: vpcEndpointIds,
	})
	if err != nil {
		return fmt.Errorf("unable to reject pec for pls %s: %s", serviceId, err)
	}

	for _, s := range output.Unsuccessful {
		return fmt.Errorf("unable to reject pec for pls %s: %s", serviceId, *s.Error.Message)
	}

	return nil
}
