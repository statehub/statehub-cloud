package aws

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go-v2/feature/ec2/imds"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	log "github.com/sirupsen/logrus"

	"gitlab.com/statehub/statehub-cloud/pkg/helpers"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

const (
	availabilityZone      = "placement/availability-zone"
	availabilityZoneID    = "placement/availability-zone-id"
	region                = "placement/region"
	networkInterfacesMacs = "network/interfaces/macs"
	filesCredentialsPath  = "/tmp/awscreds"
)

type AWS struct {
	ctx    context.Context
	log    *log.Entry
	region string
}

/***
 *	Public methods
 */

// New client
func New() (aws *AWS, err error) {
	ctx := context.TODO()

	region, ok := os.LookupEnv("AWS_REGION")
	if !ok {
		imdsC := imds.New(imds.Options{})
		out, err := imdsC.GetRegion(ctx, &imds.GetRegionInput{})
		if err != nil {
			return aws, err
		}
		region = out.Region
	}

	return &AWS{
		ctx:    ctx,
		log:    log.WithFields(log.Fields{"cloud": "aws"}),
		region: region,
	}, nil
}

/*
	Public methods
*/

func (c *AWS) GetProvider() types.Vendor {
	return types.Aws
}

func (aws *AWS) GetMetadata() (metadata types.Metadata, err error) {
	instance, err := InstanceMetadata()
	if err != nil {
		return metadata, err
	}

	accountID, err := aws.accountID()
	if err != nil {
		return metadata, err
	}

	return types.Metadata{
		InstanceID:       instance.InstanceID,
		AvailabilityZone: instance.Placement.AvailabilityZone,
		Subnet:           instance.Subnets()[0],
		Vnet:             instance.VPCs()[0],
		Region:           aws.region,
		CloudProvider:    string(types.Aws),
		ProviderScope:    accountID,
		ResourceGroup:    accountID,
	}, nil
}

func (aws *AWS) GetCallerIdentity() (*string, error) {
	stsSvc := sts.New(session.Must(session.NewSession()))
	result, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			switch awsErr.Code() {
			default:
				return result.Arn, nil
			}
		} else {
			return nil, awsErr
		}
	}

	return result.Arn, nil
}

func (aws *AWS) LoadCredentials() {
	creds, err := helpers.LoadCredentialsFromFiles(filesCredentialsPath, aws.log)
	if err != nil {
		aws.log.Errorf("Failed to load credentials from files: %w", err)
		return
	}

	for key, value := range creds {
		if value != nil {
			os.Setenv(key, *value)
		}
	}
}

func (aws *AWS) CreatePrivateLinkService(req types.PlsRequest) (res types.PlsResponse, err error) {
	plsService, err := aws.preparePlsService(req)
	if err != nil {
		return res, fmt.Errorf("failed to initialize pls service: %w", err)
	}

	return plsService.Create()
}

func (aws *AWS) DeletePrivateLinkService(req types.PlsRequest) error {
	plsService, err := aws.preparePlsService(req)
	if err != nil {
		return fmt.Errorf("failed to initialize pls service: %w", err)
	}

	return plsService.Delete()
}

func (aws *AWS) GetPrivateLinkServiceResources(req types.PlsRequest) (res types.PlsResourcesResponse, err error) {
	plsService, err := aws.preparePlsService(req)
	if err != nil {
		return res, fmt.Errorf("failed to initialize pls service: %w", err)
	}

	return plsService.GetResources()
}

/*
	Unknown public methods
*/
// subnets - get subnet ids
func (aws *AWS) Subnets() (subnets []string) {
	svc := newMetadataService()

	var subnet string
	macs := svc.getInstanceMetadata(networkInterfacesMacs)

	for _, mac := range strings.Fields(macs) {
		subnet = svc.getInstanceMetadata(networkInterfacesMacs + mac + "/subnet-id")
		subnets = append(subnets, subnet)
	}

	return subnets
}

/***
 *	Private methods
 */

// AccountID - get the account ID of the running instance
func (aws *AWS) accountID() (accountId string, err error) {
	dynamic, err := loadDynamicData()
	if err != nil {
		return accountId, err
	}

	accountId = dynamic.InstanceIdentity.Document.AccountID

	return
}

func (aws *AWS) preparePlsService(req types.PlsRequest) (service plsService, err error) {
	clients, err := aws.initClients(aws.ctx)
	if err != nil {
		return service, fmt.Errorf("failed to initialize clients: %w", err)
	}

	service = initPlsService(aws.ctx, clients, req, aws.log)

	return
}
