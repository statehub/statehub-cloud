package aws

import (
	"context"
	"crypto/sha256"
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	awsTypes "github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

func (aws *AWS) GetEndpoint(request *types.CreateEndpointRequest) (*types.Endpoint, error) {
	ec2Client, err := aws.newEc2Client()
	if err != nil {
		return nil, err
	}

	result, err := getAWSEndpointByState(aws.ctx, ec2Client, request.StateID, request.Subnet)
	if err != nil {
		return nil, err
	}
	if len(result.VpcEndpoints) == 0 {
		return nil, nil
	}
	address, err := getAdressForEndpoint(aws.ctx, ec2Client, result)
	if err != nil {
		return nil, err
	}

	statehubSecGroupID := ""
	for _, group := range result.VpcEndpoints[0].Groups {
		describeSecurityGroupInput := &ec2.DescribeSecurityGroupsInput{
			GroupIds: []string{
				*group.GroupId,
			},
		}
		describeSecurityGroupOutput, err := ec2Client.DescribeSecurityGroups(aws.ctx, describeSecurityGroupInput)
		if err != nil {
			return nil, err
		}
		aws.log.Infof("describe security group got: %+s", describeSecurityGroupOutput.SecurityGroups[0].GroupId)
		for _, tag := range describeSecurityGroupOutput.SecurityGroups[0].Tags {
			aws.log.Infof("handeling security group tag, key: %s, value: %s", *tag.Key, *tag.Value)
			aws.log.Infof("reflecting tag, key: %s, value:%s. owner tag, key: %s, value: %s,", *tag.Key, *tag.Value, *ownerTag.Key, *ownerTag.Value)
			if *tag.Key == *ownerTag.Key && *tag.Value == *ownerTag.Value {
				statehubSecGroupID = *group.GroupId
				aws.log.Infof("found statehub security group: %s", statehubSecGroupID)
			} else {
				aws.log.Infof("tag %+s, doesn't match statehub owner tag: %+s", tag, ownerTag)
			}
		}
	}

	return &types.Endpoint{
		Address:         address,
		Ready:           result.VpcEndpoints[0].State == "available",
		ID:              *result.VpcEndpoints[0].VpcEndpointId,
		SecurityGroupID: statehubSecGroupID,
	}, nil
}

func (aws *AWS) CreateEndpoint(request *types.CreateEndpointRequest) (*types.Endpoint, error) {
	ec2Client, err := aws.newEc2Client()
	if err != nil {
		return nil, err
	}

	return createAWSEndpoint(context.TODO(), &createEndpointRequest{
		ec2Client:  ec2Client,
		dryRun:     request.DryRun,
		stateName:  request.StateName,
		stateID:    request.StateID,
		vpcID:      request.Vnet,
		subnet:     request.Subnet,
		serviceURI: request.ServiceURI,
	})
}

/*
1. Delete Endpoint if exists
2. Delete security group
If we fail we fail
*/
func (awsInstance *AWS) DeleteEndpoint(ctx context.Context, endpointID string) error {
	ec2Client, err := awsInstance.newEc2Client()
	if err != nil {
		return err
	}

	// Check if endpoint exists
	describeInput := &ec2.DescribeVpcEndpointsInput{
		VpcEndpointIds: []string{endpointID},
	}
	describeOutput, err := ec2Client.DescribeVpcEndpoints(ctx, describeInput)
	if err != nil {
		return err
	}
	if len(describeOutput.VpcEndpoints) > 0 {
		// Delete endpoint
		deleteInput := &ec2.DeleteVpcEndpointsInput{
			VpcEndpointIds: []string{endpointID},
		}
		deleteOutput, err := ec2Client.DeleteVpcEndpoints(ctx, deleteInput)
		if err != nil {
			if strings.Contains(err.Error(), "InvalidVpcEndpointId.NotFound") {
				return nil
			}
			return err
		}
		if len(deleteOutput.Unsuccessful) > 0 {
			errorString := ""
			for _, unsuccessfulRequest := range deleteOutput.Unsuccessful {
				if *unsuccessfulRequest.Error.Code == "InvalidVpcEndpointId.NotFound" {
					return nil
				}
				errorString = fmt.Sprintln(errorString, unsuccessfulRequest.Error)
			}
			return fmt.Errorf(errorString)
		}
	}

	return nil
}

func (awsInstance *AWS) DeleteSecurityGroup(ctx context.Context, securityGroupID string) error {
	ec2Client, err := awsInstance.newEc2Client()
	if err != nil {
		return err
	}

	DeleteInput := &ec2.DeleteSecurityGroupInput{
		GroupId: &securityGroupID,
	}

	if _, err = ec2Client.DeleteSecurityGroup(ctx, DeleteInput); err != nil {
		if strings.Contains(err.Error(), "InvalidSecurityGroupID.NotFound") {
			return nil
		}
		return err
	}

	return nil
}

var (
	ownerTag = awsTypes.Tag{
		Key:   aws.String("owner"),
		Value: aws.String("statehub"),
	}
	baseTags = []awsTypes.Tag{
		ownerTag,
	}
)

type createEndpointRequest struct {
	ec2Client  Ec2Client
	dryRun     bool
	stateID    string
	stateName  string
	vpcID      string
	subnet     string
	serviceURI string
}

func createAWSEndpoint(ctx context.Context, request *createEndpointRequest) (*types.Endpoint, error) {
	endpointTags := append(baseTags, awsTypes.Tag{
		Key:   aws.String("state"),
		Value: aws.String(request.stateID),
	},
		awsTypes.Tag{
			Key:   aws.String("subnet"),
			Value: aws.String(request.subnet),
		},
	)

	secGroupTags := append(baseTags,
		awsTypes.Tag{
			Key:   aws.String("state"),
			Value: aws.String(request.stateID),
		},
		awsTypes.Tag{
			Key:   aws.String("vpc"),
			Value: aws.String(request.vpcID),
		},
	)

	var err error

	secGroupId, err := getOrCreateSecurityGroupID(ctx, request, secGroupTags)
	if err != nil {
		return nil, err
	}

	endpointID, err := getOrCreateEndpointID(ctx, request, endpointTags, secGroupId)
	if err != nil {
		return nil, err
	}

	describeEndpointsOutput, err := describeAWSEndpointByID(ctx, request.ec2Client, endpointID)
	if err != nil {
		return nil, err
	}

	address, err := getAdressForEndpoint(ctx, request.ec2Client, describeEndpointsOutput)
	if err != nil {
		return nil, err
	}

	endpoint := &types.Endpoint{
		Ready:           false,
		Address:         address,
		SecurityGroupID: secGroupId,
		ID:              endpointID,
	}

	return endpoint, nil
}

func getAWSEndpointByState(ctx context.Context, ec2Client Ec2Client, stateID string, subnet string) (*ec2.DescribeVpcEndpointsOutput, error) {

	DescribeVpcEndpointsInput := &ec2.DescribeVpcEndpointsInput{
		Filters: []awsTypes.Filter{
			{
				Name:   aws.String("tag:state"),
				Values: []string{stateID},
			},
			{
				Name:   aws.String("tag:owner"),
				Values: []string{"statehub"},
			},
			{
				Name:   aws.String("tag:subnet"),
				Values: []string{subnet},
			},
		},
	}

	return ec2Client.DescribeVpcEndpoints(ctx, DescribeVpcEndpointsInput)
}

func describeAWSEndpointByID(ctx context.Context, ec2Client Ec2Client, id string) (*ec2.DescribeVpcEndpointsOutput, error) {

	DescribeVpcEndpointsInput := &ec2.DescribeVpcEndpointsInput{
		VpcEndpointIds: []string{
			id,
		},
	}

	return ec2Client.DescribeVpcEndpoints(ctx, DescribeVpcEndpointsInput)
}

func generateSecurityGroupName(stateID string) string {
	return fmt.Sprintf("statehub-%s-sg", stateID)
}

func createSecurityGroupForState(ctx context.Context, request *createEndpointRequest, tags []awsTypes.Tag) (*ec2.CreateSecurityGroupOutput, error) {
	createSecurityGroupInput := &ec2.CreateSecurityGroupInput{
		DryRun:      aws.Bool(request.dryRun),
		GroupName:   aws.String(generateSecurityGroupName(request.stateID)),
		VpcId:       aws.String(request.vpcID),
		Description: aws.String("statehub security group"),
		TagSpecifications: []awsTypes.TagSpecification{
			{
				ResourceType: awsTypes.ResourceTypeSecurityGroup,
				Tags:         tags,
			},
		},
	}

	return request.ec2Client.CreateSecurityGroup(ctx, createSecurityGroupInput)
}

// TODO: Delete this function as it has no use
func getSecGroupByState(ctx context.Context, ec2Client Ec2Client, stateID string, vpcID string) (*ec2.DescribeSecurityGroupsOutput, error) {

	DescribeSecurityGroupsInput := &ec2.DescribeSecurityGroupsInput{
		Filters: []awsTypes.Filter{
			{
				Name:   aws.String("tag:state"),
				Values: []string{stateID},
			},
			{
				Name:   aws.String("tag:vpc"),
				Values: []string{vpcID},
			},
			{
				Name:   aws.String("tag:owner"),
				Values: []string{"statehub"},
			},
		},
	}

	return ec2Client.DescribeSecurityGroups(ctx, DescribeSecurityGroupsInput)
}

func getOrCreateSecurityGroupID(ctx context.Context, request *createEndpointRequest, tags []awsTypes.Tag) (string, error) {
	secGroupDesc, _ := getSecGroupByState(ctx, request.ec2Client, request.stateID, request.vpcID)

	if secGroupDesc == nil || len(secGroupDesc.SecurityGroups) == 0 {
		secGroupCreate, err := createSecurityGroupForState(ctx, request, tags)
		if err != nil {
			return "", err
		}
		secGroupId := secGroupCreate.GroupId
		err = addIngressForSecGroup(ctx, secGroupId, request.ec2Client)
		if err != nil {
			return "", err
		}
		return *secGroupId, nil
	} else {

		// We check if the ingress rule does exists
		var foundRule = false
		for _, rule := range secGroupDesc.SecurityGroups[0].IpPermissions {
			if *rule.FromPort == 3260 && *rule.ToPort == 4260 && *rule.IpProtocol == "tcp" {
				for _, iprange := range rule.IpRanges {
					if *iprange.CidrIp == "0.0.0.0/0" {
						foundRule = true
					}
				}
			}
		}

		secGroupId := secGroupDesc.SecurityGroups[0].GroupId
		if !foundRule {
			err := addIngressForSecGroup(ctx, secGroupId, request.ec2Client)
			if err != nil {
				return "", err
			}
		}

		return *secGroupId, nil
	}
}

func getOrCreateEndpointID(ctx context.Context, request *createEndpointRequest, tags []awsTypes.Tag, secGroupId string) (string, error) {
	endpointDesc, _ := getAWSEndpointByState(ctx, request.ec2Client, request.stateID, request.subnet)
	if endpointDesc == nil || len(endpointDesc.VpcEndpoints) == 0 {
		endpointCreate, err := createEndpointForState(ctx, request, tags, secGroupId)
		if err != nil {
			return "", err
		}
		endpointId := *endpointCreate.VpcEndpoint.VpcEndpointId
		return endpointId, nil
	} else {
		endpointId := *endpointDesc.VpcEndpoints[0].VpcEndpointId
		return endpointId, nil
	}
}

func createEndpointForState(ctx context.Context, request *createEndpointRequest, tags []awsTypes.Tag, secGroupId string) (*ec2.CreateVpcEndpointOutput, error) {
	//TOOD generate token
	clientToken := fmt.Sprintf("%s-%s-%s", request.stateID, request.subnet, request.serviceURI)
	createVpcEndpointInput := &ec2.CreateVpcEndpointInput{
		ClientToken: aws.String(fmt.Sprintf("%x", sha256.Sum256([]byte(clientToken)))),
		DryRun:      aws.Bool(request.dryRun),
		ServiceName: aws.String(request.serviceURI),
		VpcId:       aws.String(request.vpcID),
		TagSpecifications: []awsTypes.TagSpecification{
			{
				ResourceType: "vpc-endpoint",
				Tags:         tags,
			},
		},
		VpcEndpointType:  awsTypes.VpcEndpointTypeInterface,
		SecurityGroupIds: []string{secGroupId},
		SubnetIds:        []string{request.subnet},
	}

	return request.ec2Client.CreateVpcEndpoint(ctx, createVpcEndpointInput)
}

func getAdressForEndpoint(ctx context.Context, ec2Client Ec2Client, describeOutput *ec2.DescribeVpcEndpointsOutput) (string, error) {
	interfaceOutput, err := ec2Client.DescribeNetworkInterfaces(ctx, &ec2.DescribeNetworkInterfacesInput{
		NetworkInterfaceIds: []string{describeOutput.VpcEndpoints[0].NetworkInterfaceIds[0]},
	})
	if err != nil {
		return "", err
	}
	return *interfaceOutput.NetworkInterfaces[0].PrivateIpAddresses[0].PrivateIpAddress, nil
}

func addIngressForSecGroup(ctx context.Context, groupId *string, ec2Client Ec2Client) error {
	createSecRules := &ec2.AuthorizeSecurityGroupIngressInput{
		GroupId:    groupId,
		FromPort:   aws.Int32(3260),
		ToPort:     aws.Int32(4260),
		IpProtocol: aws.String("tcp"),
		CidrIp:     aws.String("0.0.0.0/0"),
	}

	_, err := ec2Client.AuthorizeSecurityGroupIngress(ctx, createSecRules)
	return err
}
