package aws

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials/ec2rolecreds"
	"github.com/aws/aws-sdk-go-v2/feature/ec2/imds"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	elb "github.com/aws/aws-sdk-go-v2/service/elasticloadbalancingv2"
)

func (a *AWS) getAWSConfig(ctx context.Context) (cfg aws.Config, err error) {
	cfg, err = config.LoadDefaultConfig(ctx, config.WithRegion(a.region))
	if err != nil {
		cfg, err2 := config.LoadDefaultConfig(ctx,
			config.WithCredentialsProvider(ec2rolecreds.New(func(o *ec2rolecreds.Options) {
				o.Client = imds.New(imds.Options{})
			})),
			config.WithRegion(a.region),
		)
		if err2 == nil {
			return cfg, nil
		}

		if err2 != nil {
			return cfg, err2
		}
	}

	return cfg, err
}

type Ec2Client interface {
	DescribeVpcEndpointServiceConfigurations(ctx context.Context, input *ec2.DescribeVpcEndpointServiceConfigurationsInput, optFns ...func(*ec2.Options)) (*ec2.DescribeVpcEndpointServiceConfigurationsOutput, error)
	CreateVpcEndpointServiceConfiguration(ctx context.Context, input *ec2.CreateVpcEndpointServiceConfigurationInput, optFns ...func(*ec2.Options)) (*ec2.CreateVpcEndpointServiceConfigurationOutput, error)
	DeleteVpcEndpointServiceConfigurations(ctx context.Context, params *ec2.DeleteVpcEndpointServiceConfigurationsInput, optFns ...func(*ec2.Options)) (*ec2.DeleteVpcEndpointServiceConfigurationsOutput, error)
	DescribeVpcEndpointConnections(ctx context.Context, input *ec2.DescribeVpcEndpointConnectionsInput, optFns ...func(*ec2.Options)) (*ec2.DescribeVpcEndpointConnectionsOutput, error)
	AcceptVpcEndpointConnections(ctx context.Context, params *ec2.AcceptVpcEndpointConnectionsInput, optFns ...func(*ec2.Options)) (*ec2.AcceptVpcEndpointConnectionsOutput, error)
	RejectVpcEndpointConnections(ctx context.Context, params *ec2.RejectVpcEndpointConnectionsInput, optFns ...func(*ec2.Options)) (*ec2.RejectVpcEndpointConnectionsOutput, error)
	DescribeSecurityGroups(ctx context.Context, input *ec2.DescribeSecurityGroupsInput, optFns ...func(*ec2.Options)) (*ec2.DescribeSecurityGroupsOutput, error)
	DeleteSecurityGroup(ctx context.Context, params *ec2.DeleteSecurityGroupInput, optFns ...func(*ec2.Options)) (*ec2.DeleteSecurityGroupOutput, error)
	DescribeVpcEndpoints(ctx context.Context, input *ec2.DescribeVpcEndpointsInput, optFns ...func(*ec2.Options)) (*ec2.DescribeVpcEndpointsOutput, error)
	DeleteVpcEndpoints(ctx context.Context, params *ec2.DeleteVpcEndpointsInput, optFns ...func(*ec2.Options)) (*ec2.DeleteVpcEndpointsOutput, error)
	CreateSecurityGroup(ctx context.Context, params *ec2.CreateSecurityGroupInput, optFns ...func(*ec2.Options)) (*ec2.CreateSecurityGroupOutput, error)
	CreateVpcEndpoint(ctx context.Context, params *ec2.CreateVpcEndpointInput, optFns ...func(*ec2.Options)) (*ec2.CreateVpcEndpointOutput, error)
	DescribeNetworkInterfaces(ctx context.Context, input *ec2.DescribeNetworkInterfacesInput, optFns ...func(*ec2.Options)) (*ec2.DescribeNetworkInterfacesOutput, error)
	AuthorizeSecurityGroupIngress(ctx context.Context, params *ec2.AuthorizeSecurityGroupIngressInput, optFns ...func(*ec2.Options)) (*ec2.AuthorizeSecurityGroupIngressOutput, error)
}

func (aws *AWS) newEc2Client() (client Ec2Client, err error) {
	cfg, err := aws.getAWSConfig(context.TODO()) // TODO: fix it
	if err != nil {
		return client, err
	}

	return ec2.NewFromConfig(cfg), nil
}

type ElbClient interface {
	DescribeLoadBalancers(ctx context.Context, input *elb.DescribeLoadBalancersInput, optFns ...func(*elb.Options)) (*elb.DescribeLoadBalancersOutput, error)
	CreateLoadBalancer(ctx context.Context, input *elb.CreateLoadBalancerInput, optFns ...func(*elb.Options)) (*elb.CreateLoadBalancerOutput, error)
	DeleteLoadBalancer(ctx context.Context, input *elb.DeleteLoadBalancerInput, optFns ...func(*elb.Options)) (*elb.DeleteLoadBalancerOutput, error)
	DescribeTargetGroups(ctx context.Context, input *elb.DescribeTargetGroupsInput, optFns ...func(*elb.Options)) (*elb.DescribeTargetGroupsOutput, error)
	CreateTargetGroup(ctx context.Context, input *elb.CreateTargetGroupInput, optFns ...func(*elb.Options)) (*elb.CreateTargetGroupOutput, error)
	DeleteTargetGroup(ctx context.Context, input *elb.DeleteTargetGroupInput, optFns ...func(*elb.Options)) (*elb.DeleteTargetGroupOutput, error)
	DescribeListeners(ctx context.Context, input *elb.DescribeListenersInput, optFns ...func(*elb.Options)) (*elb.DescribeListenersOutput, error)
	CreateListener(ctx context.Context, input *elb.CreateListenerInput, optFns ...func(*elb.Options)) (*elb.CreateListenerOutput, error)
	DeleteListener(ctx context.Context, params *elb.DeleteListenerInput, optFns ...func(*elb.Options)) (*elb.DeleteListenerOutput, error)
	DescribeTargetHealth(ctx context.Context, input *elb.DescribeTargetHealthInput, optFns ...func(*elb.Options)) (*elb.DescribeTargetHealthOutput, error)
	RegisterTargets(ctx context.Context, input *elb.RegisterTargetsInput, optFns ...func(*elb.Options)) (*elb.RegisterTargetsOutput, error)
	DeregisterTargets(ctx context.Context, input *elb.DeregisterTargetsInput, optFns ...func(*elb.Options)) (*elb.DeregisterTargetsOutput, error)
}

type Clients struct {
	ec2 Ec2Client
	elb ElbClient
}

func (aws *AWS) initClients(ctx context.Context) (clients Clients, err error) {
	cfg, err := aws.getAWSConfig(ctx)
	if err != nil {
		return clients, err
	}

	return Clients{
		ec2: ec2.NewFromConfig(cfg),
		elb: elb.NewFromConfig(cfg),
	}, nil
}
