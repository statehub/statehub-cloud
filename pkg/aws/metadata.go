package aws

import (
	"encoding/json"
	"errors"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

const (
	instanceMetadataService = "http://169.254.169.254"
	tokenPath               = "/latest/api/token"
	tokenTTLHeader          = "X-aws-ec2-metadata-token-ttl-seconds"
	tokenHeader             = "X-aws-ec2-metadata-token"
	instanceMetadata        = "/latest/meta-data"
	dynamicData             = "/latest/dynamic"
	instanceIdentity        = "instance-identity"
	document                = "document"
)

// InstanceMetadata - load typed instance metadata
func InstanceMetadata() (instance *Instance, err error) {
	svc := newMetadataService()

	svc.token, err = svc.getToken(svc.tokenTTL)
	if err != nil {
		return nil, err
	}

	metadata := svc.load("")
	data, err := json.Marshal(metadata)
	if err != nil {
		return nil, err
	}

	instance = new(Instance)
	err = json.Unmarshal(data, instance)
	if err != nil {
		return nil, err
	}
	return instance, nil
}

// loadDynamicData - load typed dynamic data
func loadDynamicData() (*DynamicData, error) {
	svc := newMetadataService()

	text, err := svc.getDynamicData(document)
	if err != nil {
		return nil, err
	}

	document := new(Document)
	err = json.Unmarshal([]byte(text), &document)
	if err != nil {
		return nil, err
	}

	instanceIdentity := new(InstanceIdentity)
	instanceIdentity.Document = *document
	return &DynamicData{
		InstanceIdentity: *instanceIdentity,
	}, nil
}

type metadataService struct {
	request  *resty.Request
	tokenTTL int
	token    *string
}

func newMetadataService() metadataService {
	client := resty.New()

	awsUrl, exists := os.LookupEnv("AWS_METADATA_URL")
	if exists {
		client.SetHostURL(awsUrl)
	} else {
		client.SetHostURL(instanceMetadataService)
	}

	client.SetTimeout(time.Second * 1)

	return metadataService{
		request:  client.R(),
		tokenTTL: 3600,
		token:    nil,
	}
}

func (svc *metadataService) getToken(tokenTTL int) (*string, error) {
	ttl := strconv.Itoa(svc.tokenTTL)
	response, err := svc.request.SetHeader(tokenTTLHeader, ttl).Put(tokenPath)
	if err != nil {
		return nil, err
	}

	if response.IsSuccess() {
		token := response.String()
		return &token, nil
	}

	return nil, errors.New("failed to get AWS IMDSv2 token")
}

func (svc *metadataService) refreshToken() (err error) {
	if svc.token == nil {
		return nil
	}

	svc.token, err = svc.getToken(svc.tokenTTL)
	return
}

func (svc *metadataService) getRequest(prefix, path string) (res string, err error) {
	if err = svc.refreshToken(); err != nil {
		return res, err
	}

	if svc.token != nil {
		svc.request.SetHeader(tokenHeader, *svc.token)
	}

	response, err := svc.request.Get(prefix + "/" + path)
	if err != nil {
		return res, err
	}

	return response.String(), nil
}

func (svc *metadataService) getInstanceMetadata(path string) (res string) {
	res, err := svc.getRequest(instanceMetadata, path)
	if err != nil {
		res = ""
	}
	return
}

func (svc *metadataService) getDynamicData(path string) (res string, err error) {
	res, err = svc.getRequest(dynamicData+"/"+instanceIdentity, path)
	if err != nil {
		res = ""
	}
	return
}

// RecursiveLoad - load recursively all the metadata
func (svc *metadataService) RecursiveLoad() map[string]interface{} {
	return *(svc.load(""))
}

func (svc *metadataService) load(prefix string) *map[string]interface{} {
	metadata := map[string]interface{}{}
	text := svc.getInstanceMetadata(prefix)
	for _, key := range strings.Fields(text) {
		if strings.HasSuffix(key, "/") {
			metadata[strings.TrimSuffix(key, "/")] = *(svc.load(prefix + key))
		} else {
			metadata[key] = svc.getInstanceMetadata(prefix + key)
		}
	}

	return &metadata
}
