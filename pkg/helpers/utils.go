package helpers

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

func LoadCredentialsFromFiles(dir string, log *log.Entry) (creds map[string]*string, err error) {
	if _, err := os.Stat(dir); err != nil {
		return creds, fmt.Errorf("failed to load credentials from %s: %w", dir, err)
	}

	if log != nil {
		log.Info("Loading azure creds")
	}

	files, err := os.ReadDir(dir)
	if err != nil {
		return creds, fmt.Errorf("could not read dir %s: %w", dir, err)
	}

	creds = make(map[string]*string)
	for _, file := range files {
		// Load content from file
		path := fmt.Sprint(dir, "/", file.Name())
		content, err := os.ReadFile(path)
		if err != nil {
			return creds, fmt.Errorf("could not read path %s: %w", path, err)
		}

		// Check if not empty
		if len(content) > 0 {
			if log != nil {
				log.Info(fmt.Sprintf("Adding credential variable %s", file.Name()))
			}

			value := string(content)
			creds[file.Name()] = &value
		} else {
			creds[file.Name()] = nil
		}
	}

	return
}

func StrContains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

// Because pointers cannot be compared
func IsEqStrings(a, b *string) bool {
	return *a == *b
}

// Because pointers cannot be compared
func IsEqInt32(a, b *int32) bool {
	return *a == *b
}
