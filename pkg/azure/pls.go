package azure

import (
	"context"
	"fmt"
	"time"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/azure-sdk-for-go/services/compute/mgmt/2021-03-01/compute"
	"github.com/Azure/go-autorest/autorest/azure"
	"github.com/Azure/go-autorest/autorest/to"
	log "github.com/sirupsen/logrus"
	"gitlab.com/statehub/statehub-cloud/pkg/helpers"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

// TODO: Add more logs
// TODO: check all ip subnets

const lb_subnet_ip_prefix = "192.168.253.0/24"
const lb_subnet_name = "lb"

const lb_name = "lb"
const lb_fontend_ip = "192.168.253.4"

const pls_subnet_ip_prefix = "192.168.254.0/24"
const pls_subnet_name = "pls"

const pls_nic_name = "nic"
const pls_nic_ip = "192.168.254.4"

const pls_ip = "192.168.254.5"

/*
 * Public methods
 */

// CreatePrivateLinkService creates a private link service and returns its ID and the
// list of resource IDs related to it. This action is idempotent.
func (az *Azure) CreatePrivateLinkService(req types.PlsRequest) (res types.PlsResponse, err error) {
	az.log.Debug("creating private link service with request: %v", req)

	vnetParsed, err := azure.ParseResourceID(req.VpcId)
	if err != nil {
		return res, fmt.Errorf("unable to parse vpc id: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	svc := az.initServices(ctx, vnetParsed, req)

	// Virtual Network

	vnet, err := svc.vnet.updateAddressPrefixes(
		[]string{
			lb_subnet_ip_prefix,
			pls_subnet_ip_prefix,
		},
		updateAddressPrefixesAdd,
	)
	if err != nil {
		return res, err
	}

	// Subnets

	lbSubnet, err := svc.subnet.findOrCreate(lb_subnet_name, lb_subnet_ip_prefix)
	if err != nil {
		return res, err
	}

	plsSubnet, err := svc.subnet.findOrCreate(pls_subnet_name, pls_subnet_ip_prefix)
	if err != nil {
		return res, err
	}

	// Network interface
	if _, err := svc.nic.findOrCreate(plsSubnet, vnet); err != nil {
		return res, err
	}

	// Load balancer
	lb, err := svc.lb.findOrCreate(lbSubnet, vnet)
	if err != nil {
		return res, err
	}

	generalPortPool, err := svc.lb.findOrCreatePool(lb, req.GeneralPort)
	if err != nil {
		return res, err
	}

	for instanceID, port := range req.InstancesPorts {
		instancePortPool, err := svc.lb.findOrCreatePool(lb, port)
		if err != nil {
			return res, err
		}

		nicRef, err := svc.vm.getNicRef(instanceID)
		if err != nil {
			return res, err
		}

		if err := svc.nic.updateLbPools(nicRef, []network.BackendAddressPool{
			generalPortPool,
			instancePortPool,
		}); err != nil {
			return res, err
		}
	}

	// Private link service (PLS)

	pls, err := svc.pls.findOrCreate(lb, plsSubnet, vnet)
	if err != nil {
		return res, err
	}

	res.Id = to.String(pls.ID)
	res.Name = to.String(pls.Name)

	return res, nil
}

func (az *Azure) DeletePrivateLinkService(req types.PlsRequest) (err error) {
	vnetParsed, err := azure.ParseResourceID(req.VpcId)
	if err != nil {
		return fmt.Errorf("unable to parse vpc id: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	svc := az.initServices(ctx, vnetParsed, req)

	// Delete Private link service (PLS)

	if _, err := svc.pls.findAndDelete(); err != nil {
		return fmt.Errorf("unable to find pls: %v", err)
	}

	// Delete Load balancer

	if _, err := svc.lb.findAndDelete(); err != nil {
		return fmt.Errorf("unable to find lb: %v", err)
	}

	// Delete Network interface

	if _, err := svc.nic.findAndDelete(); err != nil {
		return fmt.Errorf("unable to find nic: %v", err)
	}

	// Delete Subnets

	if _, err := svc.subnet.findAndDelete(lb_subnet_name); err != nil {
		return fmt.Errorf("unable to find lb subnet: %v", err)
	}

	if _, err := svc.subnet.findAndDelete(pls_subnet_name); err != nil {
		return fmt.Errorf("unable to find pls subnet: %v", err)
	}

	// Virtual Network

	if _, err := svc.vnet.updateAddressPrefixes(
		[]string{
			lb_subnet_ip_prefix,
			pls_subnet_ip_prefix,
		},
		updateAddressPrefixesRemove,
	); err != nil {
		return fmt.Errorf("unable to update vnet address prefixes: %v", err)
	}

	return
}

// GetPrivateLinkService return private link service ID and list of resource IDs related to it.
func (az *Azure) GetPrivateLinkServiceResources(req types.PlsRequest) (res types.PlsResourcesResponse, err error) {
	vnetParsed, err := azure.ParseResourceID(req.VpcId)
	if err != nil {
		return res, fmt.Errorf("unable to parse vpc id: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	svc := az.initServices(ctx, vnetParsed, req)

	// Subnets

	lbSubnet, err := svc.subnet.find(lb_subnet_name)
	if err != nil {
		return res, fmt.Errorf("unable to find lb subnet: %v", err)
	}

	if lbSubnet != nil {
		res.AddResource(types.PlsResourceSubnet, to.String(lbSubnet.ID))
	}

	plsSubnet, err := svc.subnet.find(pls_subnet_name)
	if err != nil {
		return res, fmt.Errorf("unable to find pls subnet: %v", err)
	}

	if plsSubnet != nil {
		res.AddResource(types.PlsResourceSubnet, to.String(plsSubnet.ID))
	}

	// Network interface
	nic, err := svc.nic.find()
	if err != nil {
		return res, fmt.Errorf("unable to find nic: %v", err)
	}

	if nic != nil {
		res.AddResource(types.PlsResourceNic, to.String(nic.ID))
	}

	// Load balancer

	lb, err := svc.lb.find()
	if err != nil {
		return res, fmt.Errorf("unable to find lb: %v", err)
	}

	if lb != nil {
		res.AddResource(types.PlsResourceLB, to.String(lb.ID))
	}

	// Private link service (PLS)

	pls, err := svc.pls.find()
	if err != nil {
		return res, fmt.Errorf("unable to find pls: %v", err)
	}

	if pls != nil {
		res.AddResource(types.PlsResourcePls, to.String(pls.ID))
	}

	return res, nil
}

/*
 * Services
 */

type services struct {
	subnet subnetService
	nic    nicService
	lb     lbService
	vm     vmService
	pls    plsService
	vnet   vnetService
}

func (az *Azure) initServices(ctx context.Context, azResource azure.Resource, req types.PlsRequest) services {
	return services{
		subnet: az.newSubnetService(ctx, azResource, req),
		nic:    az.newNicService(ctx, azResource, req),
		lb:     az.newLBService(ctx, azResource, req),
		pls:    az.newPlsService(ctx, azResource, req),
		vm:     az.newVmService(ctx, azResource),
		vnet:   az.newVnetService(ctx, azResource),
	}
}

/**
 * Naming
 */

type BaseName struct {
	name string
}

func newBaseName(name string) BaseName {
	return BaseName{
		name: name,
	}
}

func (n BaseName) add(suffix string) string {
	return fmt.Sprintf("%s-%s", n.name, suffix)
}

/**
 * Subnet svc methods
 */

type subnetService struct {
	ctx      context.Context
	netId    azure.Resource
	client   network.SubnetsClient
	baseName BaseName
	log      *log.Entry
}

func (az *Azure) newSubnetService(ctx context.Context, netId azure.Resource, req types.PlsRequest) subnetService {
	return subnetService{
		ctx:      ctx,
		netId:    netId,
		client:   az.newSubnetClient(netId.SubscriptionID),
		baseName: newBaseName(req.Name),
		log:      az.log.WithField("service", "subnet"),
	}
}

func (svc *subnetService) findOrCreate(subnetName string, addressPrefix string) (subnet network.Subnet, err error) {
	foundSubnet, err := svc.find(subnetName)
	if err != nil {
		return subnet, err
	}

	if foundSubnet != nil {
		subnet = *foundSubnet
	} else {
		subnet, err = svc.create(subnetName, addressPrefix)
	}

	return subnet, err
}

func (svc *subnetService) findAndDelete(name string) (resource *network.Subnet, err error) {
	resource, err = svc.find(name)
	if err != nil {
		return nil, err
	}

	if resource == nil {
		return
	}

	if err = svc.delete(name); err != nil {
		return
	}

	return nil, nil
}

func (svc *subnetService) find(name string) (*network.Subnet, error) {
	resource, err := svc.client.Get(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.netId.ResourceName,
		svc.baseName.add(name),
		"",
	)
	if err != nil {
		if isNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &resource, nil
}

func (svc *subnetService) create(name string, addressPrefix string) (resource network.Subnet, err error) {
	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.netId.ResourceName,
		svc.baseName.add(name),
		network.Subnet{
			SubnetPropertiesFormat: &network.SubnetPropertiesFormat{
				AddressPrefix:                     &addressPrefix,
				PrivateEndpointNetworkPolicies:    network.VirtualNetworkPrivateEndpointNetworkPoliciesEnabled,
				PrivateLinkServiceNetworkPolicies: network.VirtualNetworkPrivateLinkServiceNetworkPoliciesDisabled,
			},
		})
	if err != nil {
		return resource, err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return resource, err
	}

	return future.Result(svc.client)
}

func (svc *subnetService) delete(name string) (err error) {
	future, err := svc.client.Delete(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.netId.ResourceName,
		svc.baseName.add(name),
	)
	if err != nil {
		return err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return err
	}

	_, err = future.Result(svc.client)
	return err
}

/**
 * Vnet svc methods
 */

type vnetService struct {
	ctx    context.Context
	netId  azure.Resource
	client network.VirtualNetworksClient
	log    *log.Entry
}

func (az *Azure) newVnetService(ctx context.Context, netId azure.Resource) vnetService {
	return vnetService{
		ctx:    ctx,
		netId:  netId,
		client: az.newVnetClient(netId.SubscriptionID),
		log:    az.log.WithField("service", "virtualnetwork"),
	}
}

type updateAddressPrefixesAction string

const (
	updateAddressPrefixesAdd    updateAddressPrefixesAction = "add"
	updateAddressPrefixesRemove updateAddressPrefixesAction = "remove"
)

func (svc *vnetService) updateAddressPrefixes(prefixes []string, action updateAddressPrefixesAction) (vnet network.VirtualNetwork, err error) {
	foundVnet, err := svc.find()
	if err != nil {
		return vnet, err
	}

	if foundVnet == nil {
		return vnet, fmt.Errorf("vnet not found")
	}

	vnet = *foundVnet

	addressSpaces := foundVnet.AddressSpace
	if addressSpaces == nil {
		addressSpaces = &network.AddressSpace{
			AddressPrefixes: &[]string{},
		}
	}

	addressPrefixes := []string{}
	if addressSpaces.AddressPrefixes == nil {
		addressPrefixes = []string{}
	} else {
		addressPrefixes = *addressSpaces.AddressPrefixes
	}

	switch action {
	case updateAddressPrefixesAdd:
		for _, prefix := range prefixes {
			if !helpers.StrContains(addressPrefixes, prefix) {
				addressPrefixes = append(addressPrefixes, prefix)
			}
		}
	case updateAddressPrefixesRemove:
		previousPrefixes := []string{}
		for _, addressPrefixe := range addressPrefixes {
			if !helpers.StrContains(prefixes, addressPrefixe) {
				previousPrefixes = append(previousPrefixes, addressPrefixe)
			}
		}
		addressPrefixes = previousPrefixes
	}

	foundVnet.AddressSpace.AddressPrefixes = &addressPrefixes

	return svc.update(*foundVnet)
}

func (svc *vnetService) find() (*network.VirtualNetwork, error) {
	resource, err := svc.client.Get(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.netId.ResourceName,
		"",
	)
	if err != nil {
		if isNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &resource, nil
}

func (svc *vnetService) update(vnet network.VirtualNetwork) (resource network.VirtualNetwork, err error) {
	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.netId.ResourceName,
		vnet,
	)
	if err != nil {
		return resource, err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return resource, err
	}

	return future.Result(svc.client)
}

/**
 * Network Interface svc methods
 */

type nicService struct {
	ctx      context.Context
	netId    azure.Resource
	client   network.InterfacesClient
	baseName BaseName
	tags     map[string]*string
	log      *log.Entry
}

func (az *Azure) newNicService(ctx context.Context, netId azure.Resource, req types.PlsRequest) nicService {
	return nicService{
		ctx:      ctx,
		netId:    netId,
		client:   az.newNicClient(netId.SubscriptionID),
		baseName: newBaseName(req.Name),
		tags:     az.ToTags(req.Tags),
		log:      az.log.WithField("service", "networkinterface"),
	}
}

func (svc *nicService) findOrCreate(subnet network.Subnet, vnet network.VirtualNetwork) (resource network.Interface, err error) {
	found, err := svc.find()
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.create(subnet, vnet)
	}

	return resource, err
}

func (svc *nicService) findAndDelete() (*network.Interface, error) {
	resource, err := svc.find()
	if err != nil {
		return nil, err
	}

	if resource == nil {
		return nil, nil
	}

	if err = svc.delete(); err != nil {
		return resource, err
	}

	return nil, nil
}

func (svc *nicService) find() (*network.Interface, error) {
	resource, err := svc.client.Get(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(pls_nic_name),
		"",
	)
	if err != nil {
		if isNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &resource, nil
}

func (svc *nicService) create(subnet network.Subnet, vnet network.VirtualNetwork) (resource network.Interface, err error) {
	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(pls_nic_name),
		network.Interface{
			Location: vnet.Location,
			InterfacePropertiesFormat: &network.InterfacePropertiesFormat{
				EnableIPForwarding:          to.BoolPtr(false),
				EnableAcceleratedNetworking: to.BoolPtr(false),
				IPConfigurations: &[]network.InterfaceIPConfiguration{
					{
						Name: to.StringPtr("pls-ipconfig"),
						InterfaceIPConfigurationPropertiesFormat: &network.InterfaceIPConfigurationPropertiesFormat{
							PrivateIPAddressVersion:   network.IPVersionIPv4,
							PrivateIPAllocationMethod: network.IPAllocationMethodStatic,
							Primary:                   to.BoolPtr(true),
							PrivateIPAddress:          to.StringPtr(pls_nic_ip),
							Subnet:                    &subnet,
						},
					},
				},
			},
			Tags: svc.tags,
		})
	if err != nil {
		return resource, err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return resource, err
	}

	return future.Result(svc.client)
}

func (svc *nicService) delete() (err error) {
	future, err := svc.client.Delete(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(pls_nic_name),
	)
	if err != nil {
		return err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return err
	}

	_, err = future.Result(svc.client)
	return err
}

func (svc *nicService) updateLbPools(nicRef compute.NetworkInterfaceReference, pools []network.BackendAddressPool) (err error) {
	svc.log.Debugf("Updating nic %s with %d lb pools", nicRef.ID, len(pools))

	if nicRef.ID == nil {
		return fmt.Errorf("NIC reference ID is nil")
	}

	nicIDParts, err := azure.ParseResourceID(*nicRef.ID)
	if err != nil {
		return err
	}

	nic, err := svc.client.Get(
		svc.ctx,
		nicIDParts.ResourceGroup,
		nicIDParts.ResourceName,
		"",
	)
	if err != nil {
		return err
	}

	if nic.InterfacePropertiesFormat == nil || nic.InterfacePropertiesFormat.IPConfigurations == nil || len(*nic.InterfacePropertiesFormat.IPConfigurations) == 0 {
		return fmt.Errorf("NIC %s has no IP configurations", *nicRef.ID)
	}

	// TODO: no bueno! Need more specifications on how to find proper IpConfiguration
	ipConfigs := *nic.InterfacePropertiesFormat.IPConfigurations
	ipConfigs[0].LoadBalancerBackendAddressPools = &pools
	nic.InterfacePropertiesFormat.IPConfigurations = &ipConfigs

	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		nicIDParts.ResourceGroup,
		nicIDParts.ResourceName,
		nic,
	)
	if err != nil {
		return err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return err
	}

	_, err = future.Result(svc.client)
	return err
}

/**
 * LoadBalancer svc methods
 */

type lbService struct {
	ctx          context.Context
	netId        azure.Resource
	client       network.LoadBalancersClient
	baseName     BaseName
	tags         map[string]*string
	ipConfigName *string
	log          *log.Entry
}

func (az *Azure) newLBService(ctx context.Context, netId azure.Resource, req types.PlsRequest) lbService {
	return lbService{
		ctx:          ctx,
		netId:        netId,
		client:       az.newLbClient(netId.SubscriptionID),
		baseName:     newBaseName(req.Name),
		tags:         az.ToTags(req.Tags),
		ipConfigName: to.StringPtr("frontend"),
		log:          az.log.WithField("service", "loadbalancer"),
	}
}

func (svc *lbService) findOrCreate(subnet network.Subnet, vnet network.VirtualNetwork) (resource network.LoadBalancer, err error) {
	found, err := svc.find()
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.create(subnet, vnet)
	}

	return resource, err
}

func (svc *lbService) findAndDelete() (*network.LoadBalancer, error) {
	resource, err := svc.find()
	if err != nil {
		return nil, err
	}

	if resource == nil {
		return nil, nil
	}

	if err = svc.delete(); err != nil {
		return resource, err
	}

	return nil, nil
}

func (svc *lbService) find() (*network.LoadBalancer, error) {
	resource, err := svc.client.Get(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(lb_name),
		"",
	)
	if err != nil {
		if isNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &resource, nil
}

func (svc *lbService) create(subnet network.Subnet, vnet network.VirtualNetwork) (resource network.LoadBalancer, err error) {
	return svc.createOrUpdate(network.LoadBalancer{
		Location: vnet.Location,
		Sku: &network.LoadBalancerSku{
			Name: network.LoadBalancerSkuNameStandard,
		},
		LoadBalancerPropertiesFormat: &network.LoadBalancerPropertiesFormat{
			FrontendIPConfigurations: &[]network.FrontendIPConfiguration{
				{
					Name: svc.ipConfigName,
					FrontendIPConfigurationPropertiesFormat: &network.FrontendIPConfigurationPropertiesFormat{
						PrivateIPAddress:          to.StringPtr(lb_fontend_ip),
						PrivateIPAddressVersion:   network.IPVersionIPv4,
						PrivateIPAllocationMethod: network.IPAllocationMethodStatic,
						Subnet:                    &subnet,
					},
				},
			},
		},
		Tags: svc.tags,
	})
}

func (svc *lbService) delete() (err error) {
	future, err := svc.client.Delete(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(lb_name),
	)
	if err != nil {
		return err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return err
	}

	_, err = future.Result(svc.client)
	return err
}

func (svc *lbService) createOrUpdate(lb network.LoadBalancer) (resource network.LoadBalancer, err error) {
	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.baseName.add(lb_name),
		lb,
	)
	if err != nil {
		return resource, err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return resource, err
	}

	return future.Result(svc.client)
}

func (svc *lbService) findOrCreatePool(lb network.LoadBalancer, port int32) (resource network.BackendAddressPool, err error) {
	found := svc.findPool(lb, port)

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createPool(lb, port)
	}

	if _, err := svc.findOrCreateRule(lb, port, resource); err != nil {
		return resource, err
	}

	return resource, err
}

func (svc *lbService) findPool(lb network.LoadBalancer, port int32) (resource *network.BackendAddressPool) {
	if lb.BackendAddressPools == nil || len(*lb.BackendAddressPools) == 0 {
		return nil
	}

	for _, p := range *lb.BackendAddressPools {
		if helpers.IsEqStrings(p.Name, svc.poolName(port)) {
			resource = &p
		}
	}

	return resource
}

func (svc *lbService) poolName(port int32) *string {
	return to.StringPtr(fmt.Sprintf("%s-%d", "backend", port))
}

func (svc *lbService) createPool(lb network.LoadBalancer, port int32) (resource network.BackendAddressPool, err error) {
	var pools []network.BackendAddressPool
	if lb.BackendAddressPools == nil || len(*lb.BackendAddressPools) == 0 {
		pools = make([]network.BackendAddressPool, 0)
	} else {
		pools = *lb.BackendAddressPools
	}

	name := svc.poolName(port)

	pools = append(pools, network.BackendAddressPool{
		Name: name,
		BackendAddressPoolPropertiesFormat: &network.BackendAddressPoolPropertiesFormat{
			LoadBalancerBackendAddresses: &[]network.LoadBalancerBackendAddress{
				{
					Name: to.StringPtr(fmt.Sprintf("%s-%s", *name, "address")),
				},
			},
		},
	})
	lb.BackendAddressPools = &pools

	updatedLb, err := svc.createOrUpdate(lb)
	if err != nil {
		return resource, err
	}

	pool := svc.findPool(updatedLb, port)
	if pool == nil {
		return resource, fmt.Errorf("newly created pool for port %d not found", port)
	}

	return *pool, nil
}

func (svc *lbService) findOrCreateRule(lb network.LoadBalancer, port int32, pool network.BackendAddressPool) (resource network.LoadBalancingRule, err error) {
	found := svc.findRule(lb, port, pool)

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.createRule(lb, port, pool)
	}

	return resource, err
}

func (svc *lbService) ruleName(port int32) *string {
	return to.StringPtr(fmt.Sprintf("%s-%d", "TCP", port))
}

func (svc *lbService) findRule(lb network.LoadBalancer, port int32, pool network.BackendAddressPool) (resource *network.LoadBalancingRule) {
	if lb.LoadBalancingRules == nil || len(*lb.LoadBalancingRules) == 0 {
		return nil
	}

	for _, r := range *lb.LoadBalancingRules {
		if helpers.IsEqStrings(r.Name, svc.ruleName(port)) {
			resource = &r
			break
		}
	}

	return resource
}

func (svc *lbService) createRule(lb network.LoadBalancer, port int32, pool network.BackendAddressPool) (resource network.LoadBalancingRule, err error) {
	var rules []network.LoadBalancingRule
	if lb.LoadBalancingRules == nil || len(*lb.LoadBalancingRules) == 0 {
		rules = make([]network.LoadBalancingRule, 0)
	} else {
		rules = *lb.LoadBalancingRules
	}

	if lb.FrontendIPConfigurations == nil || len(*lb.FrontendIPConfigurations) == 0 {
		return resource, fmt.Errorf("no frontend IP configurations found for load balancer")
	}

	ipConfiguration, err := svc.getIpConfiguration(lb)
	if err != nil {
		return resource, err
	}

	rules = append(rules, network.LoadBalancingRule{
		Name: svc.ruleName(port),
		LoadBalancingRulePropertiesFormat: &network.LoadBalancingRulePropertiesFormat{
			FrontendIPConfiguration: &network.SubResource{
				ID: ipConfiguration.ID,
			},
			FrontendPort:         to.Int32Ptr(int32(port)),
			BackendPort:          to.Int32Ptr(int32(port)),
			EnableFloatingIP:     to.BoolPtr(true),
			IdleTimeoutInMinutes: to.Int32Ptr(4),
			Protocol:             network.TransportProtocolTCP,
			EnableTCPReset:       to.BoolPtr(true),
			LoadDistribution:     network.LoadDistributionDefault,
			DisableOutboundSnat:  to.BoolPtr(false),
			BackendAddressPool: &network.SubResource{
				ID: pool.ID,
			},
		},
	})
	lb.LoadBalancingRules = &rules

	updatedLb, err := svc.createOrUpdate(lb)
	if err != nil {
		return resource, err
	}

	rule := svc.findRule(updatedLb, port, pool)
	if rule == nil {
		return resource, fmt.Errorf("newly created rule for port %d not found", port)
	}

	return *rule, nil
}

func (svc *lbService) getIpConfiguration(lb network.LoadBalancer) (resource network.FrontendIPConfiguration, err error) {
	if lb.FrontendIPConfigurations == nil || len(*lb.FrontendIPConfigurations) == 0 {
		return resource, fmt.Errorf("no frontend IP configuration found in load balancer")
	}

	var found *network.FrontendIPConfiguration
	for _, c := range *lb.FrontendIPConfigurations {
		if helpers.IsEqStrings(c.Name, svc.ipConfigName) {
			found = &c
			break
		}
	}

	if found == nil {
		return resource, fmt.Errorf("no frontend IP configuration named %s found in load balancer", *svc.ipConfigName)
	}

	return *found, err
}

/**
 * Virtual Machines svc methods
 */

type vmService struct {
	ctx    context.Context
	netId  azure.Resource
	client compute.VirtualMachinesClient
	log    *log.Entry
}

func (az *Azure) newVmService(ctx context.Context, netId azure.Resource) vmService {
	return vmService{
		ctx:    ctx,
		netId:  netId,
		client: az.newVmClient(netId.SubscriptionID),
		log:    log.WithField("service", "virtualmachine"),
	}
}

func (svc *vmService) getNicRef(vmID string) (resource compute.NetworkInterfaceReference, err error) {
	vmIDParts, err := azure.ParseResourceID(vmID)
	if err != nil {
		return resource, err
	}

	vm, err := svc.client.Get(svc.ctx, vmIDParts.ResourceGroup, vmIDParts.ResourceName, "")
	if err != nil {
		return resource, err
	}

	if vm.NetworkProfile == nil || vm.NetworkProfile.NetworkInterfaces == nil || len(*vm.NetworkProfile.NetworkInterfaces) == 0 {
		return resource, fmt.Errorf("no network interfaces found for VM %s", vmID)
	}

	// TODO: getting first one, but need to find specific NIC
	resource = (*vm.NetworkProfile.NetworkInterfaces)[0]

	svc.log.Debugf("found NIC %s for VM %s", *resource.ID, vmID)

	return
}

/**
 * Pls svc methods
 */

type plsService struct {
	ctx    context.Context
	netId  azure.Resource
	client network.PrivateLinkServicesClient
	name   string
	tags   map[string]*string
	log    *log.Entry
}

func (az *Azure) newPlsService(ctx context.Context, netId azure.Resource, req types.PlsRequest) plsService {
	return plsService{
		ctx:    ctx,
		netId:  netId,
		client: az.newPlsClient(netId.SubscriptionID),
		name:   req.Name,
		tags:   az.ToTags(req.Tags),
		log:    log.WithField("service", "privatelinkservice"),
	}
}

func (svc *plsService) findOrCreate(lb network.LoadBalancer, subnet network.Subnet, vnet network.VirtualNetwork) (resource network.PrivateLinkService, err error) {
	found, err := svc.find()
	if err != nil {
		return resource, err
	}

	if found != nil {
		resource = *found
	} else {
		resource, err = svc.create(lb, subnet, vnet)
	}

	return resource, err
}

func (svc *plsService) findAndDelete() (*network.PrivateLinkService, error) {
	resource, err := svc.find()
	if err != nil {
		return nil, err
	}

	if resource == nil {
		return nil, nil
	}

	if err = svc.findAndrejectAllPec(); err != nil {
		return resource, err
	}

	if err = svc.delete(); err != nil {
		return resource, err
	}

	return nil, nil
}

func (svc *plsService) find() (*network.PrivateLinkService, error) {
	resource, err := svc.client.Get(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.name,
		"",
	)
	if err != nil {
		if isNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &resource, nil
}

func (svc *plsService) create(lb network.LoadBalancer, subnet network.Subnet, vnet network.VirtualNetwork) (resource network.PrivateLinkService, err error) {
	if lb.FrontendIPConfigurations == nil || len(*lb.FrontendIPConfigurations) == 0 {
		return resource, fmt.Errorf("no frontend IP configuration found for load balancer")
	}

	future, err := svc.client.CreateOrUpdate(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.name,
		network.PrivateLinkService{
			Location: vnet.Location,
			PrivateLinkServiceProperties: &network.PrivateLinkServiceProperties{
				EnableProxyProtocol: to.BoolPtr(false),
				LoadBalancerFrontendIPConfigurations: &[]network.FrontendIPConfiguration{
					(*lb.FrontendIPConfigurations)[0], // TODO: getting first one, but need to find specific ip config
				},
				IPConfigurations: &[]network.PrivateLinkServiceIPConfiguration{
					{
						Name: to.StringPtr("ipconfig"),
						PrivateLinkServiceIPConfigurationProperties: &network.PrivateLinkServiceIPConfigurationProperties{
							PrivateIPAddress:          to.StringPtr(pls_ip),
							PrivateIPAllocationMethod: network.IPAllocationMethodStatic,
							Subnet:                    &subnet,
							Primary:                   to.BoolPtr(true),
							PrivateIPAddressVersion:   network.IPVersionIPv4,
						},
					},
				},
			},
			Tags: svc.tags,
		},
	)
	if err != nil {
		return resource, err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return resource, err
	}

	return future.Result(svc.client)
}

func (svc *plsService) delete() (err error) {
	future, err := svc.client.Delete(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.name,
	)
	if err != nil {
		return err
	}

	if err = future.WaitForCompletionRef(svc.ctx, svc.client.Client); err != nil {
		return err
	}

	_, err = future.Result(svc.client)
	return err
}

// findAndrejectAllPec is a helper function to reject all private endpoint connections
func (svc *plsService) findAndrejectAllPec() (err error) {
	pecList, err := svc.findAllPec()
	if err != nil {
		return err
	}

	for _, pec := range pecList {
		if err := svc.rejectPec(pec); err != nil {
			return err
		}
	}

	return
}

func (svc *plsService) findAllPec() (pecList []network.PrivateEndpointConnection, err error) {
	pecList = make([]network.PrivateEndpointConnection, 0)

	pecPage, err := svc.client.ListPrivateEndpointConnections(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.name,
	)

	pecList = append(pecList, pecPage.Values()...) // TODO: has pagination

	return
}

func (svc *plsService) rejectPec(pec network.PrivateEndpointConnection) (err error) {
	pec.PrivateEndpointConnectionProperties.PrivateLinkServiceConnectionState.Status = to.StringPtr("Rejected")
	pec, err = svc.client.UpdatePrivateEndpointConnection(
		svc.ctx,
		svc.netId.ResourceGroup,
		svc.name,
		*pec.Name,
		pec,
	)
	return
}
