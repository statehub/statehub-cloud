package azure

import (
	"github.com/google/uuid"
)

// Instance - Azure instance metadata
type Instance struct {
	Compute Compute `json:"compute"`
	Network Network `json:"network"`
}

// Compute - describe Azure compute element
type Compute struct {
	AzEnvironment              string    `json:"azEnvironment"`
	IsHostCompatibilityLayerVM string    `json:"isHostCompatibilityLayerVm"`
	LicenseType                string    `json:"licenseType,omitempty"`
	Location                   string    `json:"location"`
	Name                       string    `json:"name"`
	Offer                      string    `json:"offer"`
	OsProfile                  OsProfile `json:"osProfile"`
	OsType                     string    `json:"osType"`
	PlacementGroupID           string    `json:"placementGroupId"`
	// plan
	// platformUpdateDomain
	// platformFaultDomain
	// provider
	// publicKeys
	// publisher
	ResourceGroupName string `json:"resourceGroupName"`
	ResourceID        string `json:"resourceId"`
	Sku               string `json:"sku"`
	// securityProfile
	// storageProfile
	SubscriptionID uuid.UUID `json:"subscriptionId"`
	Tags           string    `json:"tags"`
	// tagsList
	Version        string    `json:"version"`
	VMID           uuid.UUID `json:"vmId"`
	VMScaleSetName string    `json:"vmScaleSetName"`
	VMSize         string    `json:"vmSize"`
	Zone           string    `json:"zone"`
}

// OsProfile - well OS Profile
type OsProfile struct {
	AdminUsername                 string `json:"adminUsername"`
	ComputerName                  string `json:"computerName"`
	DisablePasswordAuthentication string `json:"disablePasswordAuthentication"`
}

// Network description
type Network struct {
	Interface []Interface `json:"interface"`
}

// Interface description
type Interface struct {
	IPv4       IPv4   `json:"ipv4"`
	IPv6       IPv6   `json:"ipv6"`
	MacAddress string `json:"macAddress"`
}

// IPv4 - IPv4 description
type IPv4 struct {
	IPAddress []IPAddress `json:"ipAddress"`
	Subnet    []Subnet    `json:"subnet"`
}

// IPv6 - IPv4 description
type IPv6 struct {
	IPAddress []IPAddress `json:"ipAddress"`
	Subnet    Subnet      `json:"subnet"`
}

// IPAddress - IP address configuration
type IPAddress struct {
	PrivateIPAddress string `json:"privateIpAddress"`
	PublicIPAddress  string `json:"publicIpAddress"`
}

// Subnet description
type Subnet struct {
	Address string `json:"address"`
	Prefix  string `json:"prefix"`
}
