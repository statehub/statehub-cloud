package azure

import (
	"context"
	"errors"
	"os"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/profiles/latest/network/mgmt/network"
	"github.com/Azure/azure-sdk-for-go/services/compute/mgmt/2021-03-01/compute"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/azure"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	log "github.com/sirupsen/logrus"
	"gitlab.com/statehub/statehub-cloud/pkg/helpers"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

const filesCredentialsPath = "/tmp/azurecreds"

type Azure struct {
	authorizer autorest.Authorizer
	log        *log.Entry
	userAgent  string
}

// New Azure client
func New() (azure *Azure, err error) {
	// TODO: make logging optional
	log := log.WithFields(log.Fields{
		"cloud": "azure",
	})

	authorizer, err := getAuthorizerFromEnv()
	if err != nil {
		log.Errorf("Failed to get authorizer from environment variables: %w", err)

		authorizer, err = getAuthorizerFromCredentialsFiles(log)
		if err != nil {
			return azure, err
		}
	}

	// TODO: make this configurable
	userAgent := "statehub-cloud/0.0.1"

	return &Azure{authorizer, log, userAgent}, err
}

func getAuthorizerFromEnv() (authorizer autorest.Authorizer, err error) {
	settings, err := auth.GetSettingsFromEnvironment()
	if err != nil {
		return authorizer, err
	}

	return settings.GetAuthorizer()
}

func getAuthorizerFromCredentialsFiles(log *log.Entry) (authorizer autorest.Authorizer, err error) {
	log.Info("Trying to get authorizer from credentials files")

	creds, err := helpers.LoadCredentialsFromFiles(filesCredentialsPath, log)
	if err != nil {
		return authorizer, err
	}

	AZURE_CLIENT_ID, ok := creds["AZURE_CLIENT_ID"]
	if !ok {
		return authorizer, errors.New("AZURE_CLIENT_ID not found in credentials")
	}

	AZURE_CLIENT_SECRET, ok := creds["AZURE_CLIENT_SECRET"]
	if !ok {
		return authorizer, errors.New("AZURE_CLIENT_SECRET not found in credentials")
	}

	AZURE_TENANT_ID, ok := creds["AZURE_TENANT_ID"]
	if !ok {
		return authorizer, errors.New("AZURE_TENANT_ID not found in credentials")
	}

	return auth.NewClientCredentialsConfig(
		*AZURE_CLIENT_ID,
		*AZURE_CLIENT_SECRET,
		*AZURE_TENANT_ID,
	).Authorizer()
}

func (az *Azure) UserAgent() string {
	return az.userAgent
}

func (az *Azure) GetProvider() types.Vendor {
	return types.Azure
}

func (az *Azure) GetMetadata() (metadata types.Metadata, err error) {
	instanceMetadata, err := InstanceMetadata()
	if err != nil {
		return metadata, err
	}

	az.log.Info("Done InstanceMetadata")

	vnetID := os.Getenv("AZURE_VNET")
	subnetID := os.Getenv("AZURE_SUBNET")
	if subnetID == "" {
		subnetFQN, err := az.FindMySubnetID(instanceMetadata)
		if err != nil {
			return metadata, err
		}

		vnetID = strings.Split(subnetFQN, "/")[8] // TODO: create vnet fetch function for azure
		subnetID = strings.Split(subnetFQN, "/")[10]
	}

	return types.Metadata{
		InstanceID:       instanceMetadata.Compute.VMID.String(),
		Region:           instanceMetadata.Compute.Location,
		AvailabilityZone: instanceMetadata.Compute.Zone,
		CloudProvider:    string(types.Azure),
		Vnet:             vnetID,
		Subnet:           subnetID,
		ProviderScope:    instanceMetadata.Compute.SubscriptionID.String(),
		ResourceGroup:    instanceMetadata.Compute.ResourceGroupName,
	}, nil
}

func (azure *Azure) FindMySubnetID(metadata *Instance) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	config := azureConfig{
		subscriptionID: metadata.Compute.SubscriptionID.String(),
		location:       metadata.Compute.Location,
		resourceGroup:  metadata.Compute.ResourceGroupName,
		authorizer:     azure.authorizer,
		ctx:            ctx,
		log:            azure.log,
	}

	if metadata.Compute.VMScaleSetName != "" {
		return config.findSubnetIDbyVMSS(metadata.Compute.VMScaleSetName)
	} else {
		return config.findSubnetIDbyVM(metadata.Compute.Name)
	}
}

func (azure *Azure) GetCallerIdentity() (*string, error) {
	return nil, nil
}

func (azure *Azure) LoadCredentials() {
	creds, err := helpers.LoadCredentialsFromFiles(filesCredentialsPath, azure.log)
	if err != nil {
		azure.log.Errorf("Failed to load credentials from files: %w", err)
		return
	}

	for key, value := range creds {
		if value != nil {
			os.Setenv(key, *value)
		}
	}
}

func (azure *Azure) newPlsClient(subscriptionId string) (client network.PrivateLinkServicesClient) {
	client = network.NewPrivateLinkServicesClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) newLbClient(subscriptionId string) (client network.LoadBalancersClient) {
	client = network.NewLoadBalancersClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) newNicClient(subscriptionId string) (client network.InterfacesClient) {
	client = network.NewInterfacesClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) newSubnetClient(subscriptionId string) (client network.SubnetsClient) {
	client = network.NewSubnetsClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) newVnetClient(subscriptionId string) (client network.VirtualNetworksClient) {
	client = network.NewVirtualNetworksClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) newVmClient(subscriptionId string) (client compute.VirtualMachinesClient) {
	client = compute.NewVirtualMachinesClient(subscriptionId)
	client.Authorizer = azure.authorizer
	client.AddToUserAgent(azure.UserAgent())
	return
}

func (azure *Azure) ToTags(tags map[string]string) (resTags map[string]*string) {
	resTags = make(map[string]*string, len(tags))
	for k, v := range tags {
		v := v
		resTags[k] = &v
	}
	return
}

//

func isNotFoundError(err error) bool {
	azureError, ok := err.(autorest.DetailedError)
	if !ok {
		return false
	}

	if azureError.Original == nil {
		return false
	}

	origError, ok := azureError.Original.(*azure.RequestError)
	if !ok {
		return false
	}

	return origError.ServiceError.Code == "NotFound" || origError.ServiceError.Code == "ResourceNotFound"
}
