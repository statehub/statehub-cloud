package azure

import (
	"encoding/json"
	"errors"

	"github.com/go-resty/resty/v2"
)

const instanceMetadataHost = "http://169.254.169.254"
const instanceMetadataPath = "/metadata/instance"

func InstanceMetadata() (instanceMetadata *Instance, err error) {
	response, err := resty.
		New().
		SetHostURL(instanceMetadataHost).
		R().
		SetQueryParams(map[string]string{
			"format":      "json",
			"api-version": "2020-10-01",
		}).
		SetHeader("Metadata", "True").
		SetHeader("Accept", "application/json").
		Get(instanceMetadataPath)

	if err != nil {
		return nil, err
	}

	if !response.IsSuccess() {
		return nil, errors.New("failed to fetch Azure Instance Metadata")
	}

	err = json.Unmarshal(response.Body(), &instanceMetadata)
	return instanceMetadata, err
}
