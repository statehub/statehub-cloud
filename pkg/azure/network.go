package azure

import (
	// "context"
	// "time"

	"context"
	"regexp"
	"time"

	"github.com/Azure/azure-sdk-for-go/services/compute/mgmt/2021-03-01/compute"
	"github.com/Azure/azure-sdk-for-go/services/network/mgmt/2021-02-01/network"
	"github.com/Azure/go-autorest/autorest"
	log "github.com/sirupsen/logrus"
)

type azureConfig struct {
	subscriptionID string
	location       string
	resourceGroup  string
	authorizer     autorest.Authorizer
	ctx            context.Context
	log            *log.Entry
}

func (config *azureConfig) findSubnetIDbyVM(vmName string) (string, error) {

	config.log.Infof("Started findSubnetIDbyVM %s", vmName)

	vm, err := config.getVM(vmName)
	if err != nil {
		return "", err
	}

	config.log.Infof("Recived vm id %s", *vm.ID)

	nics := *vm.NetworkProfile.NetworkInterfaces
	primaryNic := *nics[0].ID
	config.log.Infof("primaryNic is %s", primaryNic)
	for _, nic := range nics {
		if *nic.Primary {
			primaryNic = *nic.ID
		}
	}
	config.log.Infof("primaryNic is now %s after looping nics", primaryNic)

	nic, err := config.getNic(primaryNic)
	if err != nil {
		return "", err
	}
	config.log.Infof("getNic ended with status code ", nic.StatusCode)

	ipConfigs := *nic.IPConfigurations
	subnet := ipConfigs[0].Subnet
	config.log.Infof("Recived subnet %s", subnet)
	return *subnet.ID, nil
}

func (config *azureConfig) findSubnetIDbyVMSS(name string) (string, error) {
	vmss, err := config.getVMSS(name)
	if err != nil {
		return "", err
	}

	configs := *vmss.VirtualMachineProfile.NetworkProfile.NetworkInterfaceConfigurations
	primaryIPConfigs := *configs[0].IPConfigurations
	for _, config := range configs {
		if config.Primary != nil && *config.Primary {
			primaryIPConfigs = *config.IPConfigurations
			break
		}
	}

	subnet := *primaryIPConfigs[0].Subnet
	for _, config := range primaryIPConfigs {
		if config.Primary != nil && *config.Primary {
			if config.Subnet != nil {
				subnet = *config.Subnet
				break
			}
		}
	}

	return *subnet.ID, nil
}

// GetVM gets the specified VM info
func (config *azureConfig) getVM(vmName string) (compute.VirtualMachine, error) {
	config.log.Infof("Started getVM %s", vmName)
	vmClient := config.getVMClient()
	return vmClient.Get(config.ctx, config.resourceGroup, vmName, compute.InstanceViewTypesInstanceView)
}

// GetVMSS gets the specified VM Scale Set info
func (config *azureConfig) getVMSS(vmssName string) (compute.VirtualMachineScaleSet, error) {
	client := config.getVMSSClient()
	return client.Get(config.ctx, config.resourceGroup, vmssName, compute.ExpandTypesForGetVMScaleSetsUserData)
}

func (config *azureConfig) getNic(nic string) (network.Interface, error) {
	nicClient := config.getNicClient()
	return nicClient.Get(config.ctx, config.resourceGroup, nic, "")

}

func (config *azureConfig) getVMClient() compute.VirtualMachinesClient {
	vmClient := compute.NewVirtualMachinesClient(config.subscriptionID)
	// a, _ := iam.GetResourceManagementAuthorizer()
	vmClient.Authorizer = config.authorizer
	vmClient.AddToUserAgent(config.UserAgent())
	config.log.Infof("Got getVMClient")
	return vmClient
}

func (config *azureConfig) getVMSSClient() compute.VirtualMachineScaleSetsClient {
	vmClient := compute.NewVirtualMachineScaleSetsClient(config.subscriptionID)
	// a, _ := iam.GetResourceManagementAuthorizer()
	vmClient.Authorizer = config.authorizer
	vmClient.AddToUserAgent(config.UserAgent())
	return vmClient
}

func (config *azureConfig) getNicClient() network.InterfacesClient {
	nicClient := network.NewInterfacesClient(config.subscriptionID)
	// auth, _ := iam.GetResourceManagementAuthorizer()
	nicClient.Authorizer = config.authorizer
	// TODO: take it from azure client library
	nicClient.AddToUserAgent(config.UserAgent())
	return nicClient
}

func (config *azureConfig) UserAgent() string {
	return "statehub-cloud/0.0.1"
}

func (azure *Azure) getInterface(interfaceID string, subscriptionID string, resourceGroup string) (*network.Interface, error) {

	client := azure.getInterfacesClient(subscriptionID)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	re, err := regexp.Compile(`networkInterfaces\/(.*)`)
	if err != nil {
		return nil, err
	}
	match := re.FindStringSubmatch(interfaceID)

	interfaceObject, err := client.Get(ctx, resourceGroup, match[1], "")
	return &interfaceObject, err
}

func (azure *Azure) getInterfacesClient(subscriptionID string) network.InterfacesClient {
	intfClient := network.NewInterfacesClient(subscriptionID)
	intfClient.Authorizer = azure.authorizer

	return intfClient
}
