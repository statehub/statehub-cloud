package azure

import (
	"context"
	"crypto/sha256"
	"fmt"
	"os"
	"time"

	"github.com/Azure/azure-sdk-for-go/services/network/mgmt/2021-02-01/network"
	"github.com/Azure/go-autorest/autorest"
	azureSDK "github.com/Azure/go-autorest/autorest/azure"

	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

func (azure *Azure) GetEndpoint(request *types.CreateEndpointRequest) (*types.Endpoint, error) {
	endpointAddress := os.Getenv("AZURE_ENDPOINT_ADDR")
	endpointID := os.Getenv("AZURE_ENDPOINT_ID")
	ready := true
	if endpointAddress == "" {

		azureRequest, err := azure.createAzureRequest(request)
		if err != nil {
			return nil, err
		}

		privateEndpoint, err := azure.getAzureEndpoint(azureRequest)
		if err != nil {
			if err.(autorest.DetailedError).Response.StatusCode == 404 {
				return nil, nil // AWS returns no error but empty result, so we going to do the same
			}
			return nil, err
		}

		// ready := false

		if privateEndpoint.ProvisioningState == network.ProvisioningStateSucceeded {
			connections := *privateEndpoint.ManualPrivateLinkServiceConnections
			if connections[0].PrivateLinkServiceConnectionProperties.ProvisioningState == network.ProvisioningStateSucceeded {
				ready = *(connections[0].PrivateLinkServiceConnectionProperties.PrivateLinkServiceConnectionState.Status) == "Approved"
			}
		}

		endpointID = *privateEndpoint.ID
		// if !ready {
		// 	return &types.Endpoint{
		// 		Address: "",
		// 		Ready:   ready,
		// 		ID:      endpointID,
		// 	}, nil
		// }

		interfaces := *privateEndpoint.NetworkInterfaces
		interfaceID := *interfaces[0].ID
		interfaceObject, err := azure.getInterface(interfaceID, request.ProviderScope, request.ResourceGroup)
		if err != nil {
			return &types.Endpoint{}, err
		}

		ipConfigs := *interfaceObject.IPConfigurations
		endpointAddress = *ipConfigs[0].PrivateIPAddress
	}

	endpoint := types.Endpoint{
		Address: endpointAddress,
		Ready:   ready,
		ID:      endpointID,
	}

	return &endpoint, nil
}

func (azure *Azure) CreateEndpoint(request *types.CreateEndpointRequest) (*types.Endpoint, error) {

	err := azure.disablePrivateLinkPolicy(request)
	if err != nil {
		return nil, err
	}

	endpointID, found := os.LookupEnv("AZURE_ENDPOINT_ID")
	if !found {
		azureRequest, err := azure.createAzureRequest(request)
		if err != nil {
			return nil, err
		}

		azureEndpoint, err := azure.createAzureEndpoint(azureRequest)
		if err != nil {
			return nil, err
		}
		endpointID = *azureEndpoint.ID
	}

	endpoint := &types.Endpoint{
		Ready: false,
		ID:    endpointID,
	}

	return endpoint, nil
}

/*
1. Delete Endpoint
*/
func (azure *Azure) DeleteEndpoint(ctx context.Context, endpointID string) error {

	resourceObject, err := azureSDK.ParseResourceID(endpointID)
	if err != nil {
		return err
	}
	client := azure.getPrivateEndpointClient(resourceObject.SubscriptionID)
	promise, err := client.Delete(ctx, resourceObject.ResourceGroup, resourceObject.ResourceName)
	if err != nil {
		return err
	}
	err = promise.WaitForCompletionRef(ctx, client.Client)
	if err != nil {
		return err
	}
	_, err = promise.Result(client)
	return err
}

type createEndpointRequest struct {
	endpointName   string
	connectionName string
	subscriptionID string
	location       string
	subnetID       string
	resourceGroup  string
	serviceID      string
	tags           map[string]*string
}

func (azure *Azure) getPrivateEndpointClient(subscriptionID string) network.PrivateEndpointsClient {
	pleClient := network.NewPrivateEndpointsClient(subscriptionID)
	pleClient.Authorizer = azure.authorizer

	return pleClient
}

func (azure *Azure) getAzureEndpoint(request *createEndpointRequest) (*network.PrivateEndpoint, error) {
	client := azure.getPrivateEndpointClient((request.subscriptionID))
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	privateEndpoint, err := client.Get(ctx, request.resourceGroup, request.endpointName, "")
	return &privateEndpoint, err
}

func (azure *Azure) createAzureEndpoint(request *createEndpointRequest) (*network.PrivateEndpoint, error) {
	client := azure.getPrivateEndpointClient(request.subscriptionID)
	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	parameters := request.getPrivateEndpointParams()
	promise, err := client.CreateOrUpdate(ctx, request.resourceGroup, request.endpointName, parameters)

	if err != nil {
		return nil, err
	}

	err = promise.WaitForCompletionRef(ctx, client.Client)

	if err != nil {
		return nil, err
	}

	privateEndpoint, err := promise.Result(client)
	return &privateEndpoint, err
}

func (request *createEndpointRequest) getPrivateEndpointParams() network.PrivateEndpoint {
	connection_props := network.PrivateLinkServiceConnectionProperties{
		PrivateLinkServiceID: &request.serviceID,
	}
	connection := network.PrivateLinkServiceConnection{
		Name:                                   &request.connectionName,
		PrivateLinkServiceConnectionProperties: &connection_props,
	}
	props := network.PrivateEndpointProperties{
		Subnet:                              &network.Subnet{ID: &request.subnetID},
		ManualPrivateLinkServiceConnections: &[]network.PrivateLinkServiceConnection{connection},
	}
	endpoint := network.PrivateEndpoint{
		PrivateEndpointProperties: &props,
		Location:                  &request.location,
		Tags:                      request.tags,
	}

	return endpoint
}

func (azure *Azure) createAzureRequest(request *types.CreateEndpointRequest) (*createEndpointRequest, error) {
	nameToken := fmt.Sprintf("%s-%s-%s", request.StateName, request.Subnet, request.ServiceURI)
	endpointName := fmt.Sprintf("statehub-ple-%x", sha256.Sum256([]byte(nameToken)))
	connectionName := fmt.Sprintf("statehub-plec-%x", sha256.Sum256([]byte(nameToken)))

	tags := make(map[string]*string)
	tags["statehub:state-name"] = &request.StateName
	tags["statehub:state-id"] = &request.StateID

	subnetId := azure.requestGetSubnetID(request)

	azureRequest := createEndpointRequest{
		endpointName:   endpointName,
		connectionName: connectionName,
		subscriptionID: request.ProviderScope,
		location:       request.Region,
		subnetID:       subnetId,
		resourceGroup:  request.ResourceGroup,
		serviceID:      request.ServiceURI,
		tags:           tags,
	}

	return &azureRequest, nil
}

func (azure *Azure) DeleteSecurityGroup(ctx context.Context, securityGroupID string) error {
	return nil
}

func (azure *Azure) getSubnetClient(subscriptionID string) network.SubnetsClient {
	subClient := network.NewSubnetsClient(subscriptionID)
	subClient.Authorizer = azure.authorizer

	return subClient
}

func (azure *Azure) disablePrivateLinkPolicy(request *types.CreateEndpointRequest) error {
	client := azure.getSubnetClient(request.ProviderScope)
	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Second)
	defer cancel()

	subnet, err := client.Get(ctx, request.ResourceGroup, request.Vnet, request.Subnet, "")

	if err != nil {
		return err
	}

	subnet.PrivateEndpointNetworkPolicies = network.VirtualNetworkPrivateEndpointNetworkPoliciesDisabled

	promise, err := client.CreateOrUpdate(ctx, request.ResourceGroup, request.Vnet, request.Subnet, subnet)

	if err != nil {
		return err
	}

	err = promise.WaitForCompletionRef(ctx, client.Client)

	if err != nil {
		return err
	}

	return nil
}

func (azure *Azure) requestGetSubnetID(request *types.CreateEndpointRequest) string {
	return fmt.Sprintf("/subscriptions/%s/resourceGroups/%s/providers/Microsoft.Network/virtualNetworks/%s/subnets/%s", request.ProviderScope, request.ResourceGroup, request.Vnet, request.Subnet)
}
