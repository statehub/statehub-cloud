# Statehub Cloud - go cloud agnostic package

## Metadata service

```go
import "gitlab.com/statehub/statehub-cloud/pkg/cloud"
cloud, err := cloud.GetCloud()
metadata, err := cloud.GetMetadata()
region := metadata.Region
```

## Endpoint creation
```go
import (
	"gitlab.com/statehub/statehub-cloud/pkg/cloud"
	"gitlab.com/statehub/statehub-cloud/pkg/types"
)

cloud, err := cloud.GetCloud()
endpointID, err := cloud.CreateEndpoint(&types.CreateEndpointRequest{
    StateID:    "87324fhb3-f4378f1h8-f34hg78",
    StateName:  "statesarecool",
    DryRun:     false,
    Vnet:       "vpc-xxxxxxxxxxx",
    Subnet:     "subnet-xxxxxxxxxxxx",
    ServiceURI: "com.example.serviceone",
})
```